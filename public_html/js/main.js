$().ready(function(){
	$('#admin-menu-buton').openCloseButon('#admin-menu');
	
});

(function($){
	
	$.fn.openCloseButon = function(closeClass){
		$(this).click(function() {
			$(closeClass).toggle();
			$(this).buttonAdminChange();
		});
	}
	
	$.fn.buttonAdminChange = function(){
		$class = $(this).attr('class');
		if($class == 'open'){
			$(this).attr('class','close');
		}
		if($class == 'close'){
			$(this).attr('class','open');
		}
	}
	
})(jQuery);