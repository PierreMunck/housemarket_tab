$().ready(function(){
	
	$('#demo').append('<div id="current-demo">1</div>');
	$('#demo-1').show();
	$('#demo-thumbnail-1').click(function() {
		cambiar(1);
	});
	
	$('#demo-thumbnail-2').click(function() {
		cambiar(2);
	});
	
	$('#demo-thumbnail-3').click(function() {
		cambiar(3);
	});
	
	$('#demo-thumbnail-4').click(function() {
		cambiar(4);
	});
	
});

function cambiar(id){
	var val = $('#current-demo').text();
	var classoldid = '#demo-' + val;
	$('#current-demo').text(id);
	var classid = '#demo-' + id;
	$(classoldid).hide();
	$(classid).show();
}