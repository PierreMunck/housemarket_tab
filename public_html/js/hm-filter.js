$().ready(function(){
		
	$('#filter').click(function() {
		$('#filter-form').toggle();
		return false;
	});
	
	$('#filter-close').click(function() {
		$('#filter-form').toggle();
		return false;
	});

	loadSelectOption('/js/precioselectval_'+ $("#ProMode").val() +'.js','#ProPrecioMin');
	loadSelectOption('/js/precioselectval_'+ $("#ProMode").val() +'.js','#ProPrecioMax');
	loadSelectOption('/js/areaselectval_'+ $('#ProAreaMesure').val() +'.js','#ProAreaMin');
	loadSelectOption('/js/areaselectval_'+ $('#ProAreaMesure').val() +'.js','#ProAreaMax');
	
	$('#ProAreaMesure').change(function() {
		$('#ProAreaMin').empty().append('<option value=""  >Min</option>');
		$('#ProAreaMax').empty().append('<option value=""  >Max</option>');

		loadSelectOption('/js/areaselectval_'+ $('#ProAreaMesure').val() +'.js','#ProAreaMin');
		loadSelectOption('/js/areaselectval_'+ $('#ProAreaMesure').val() +'.js','#ProAreaMax');
	});
	
	$('#Protype').change(function() {
		filtrar();
	});
	
	$('#ProPrecioMin').change(function() {
		filtrar();
	});
	
	$('#ProPrecioMax').change(function() {
		filtrar();
	});
	
	$('#ProAreaMin').change(function() {
		filtrar();
	});
	
	$('#ProAreaMax').change(function() {
		filtrar();
	});
	
	$('#ProCuarto').change(function() {
		filtrar();
	});
	
	$('#ProBano').change(function() {
		filtrar();
	});
	
});

function loadSelectOption(url,obj){
	$.getJSON(url, function(data) {
	  var items = [];
	  $.each(data, function(key, val) {
	    $(obj).append('<option value="' + key + '"  >' + val + '</option>');
	  });
	});
}

function filtrar(){
	var url = $("#UrlFilter").val();
	//var url = "/application/filter/propiedadbypage/"+ $("#ProId").val() +"/"+ $("#ProMode").val();
	var param  = ""
	if($('#Protype').val() != ""){
		if(param != ""){
			param += "&";
		}
		param += "Type="+$('#Protype').val();
	}
	//ProPrecioMin
	if($('#ProPrecioMin').val() != ""){
		if(param != ""){
			param += "&";
		}
		param += "PrecioMin="+$('#ProPrecioMin').val();
	}
	//ProPrecioMax
	if($('#ProPrecioMax').val() != ""){
		if(param != ""){
			param += "&";
		}
		param += "PrecioMax="+$('#ProPrecioMax').val();
	}
	//ProAreaMin
	if($('#ProAreaMin').val() != ""){
		if(param != ""){
			param += "&";
		}
		param += "AreaMin="+$('#ProAreaMin').val();
	}
	//ProAreaMax
	if($('#ProAreaMax').val() != ""){
		if(param != ""){
			param += "&";
		}
		param += "AreaMax="+$('#ProAreaMax').val();
	}
	//ProCuarto
	if($('#ProCuarto').val() != ""){
		if(param != ""){
			param += "&";
		}
		param += "Cuarto="+$('#ProCuarto').val();
	}
	//ProBano
	if($('#ProBano').val() != ""){
		if(param != ""){
			param += "&";
		}
		param += "Bano="+$('#ProBano').val();
	}
	
	if(param != ""){
		url += "?"+param;
	}
	
	$.getJSON(url,null,reloadMap);
	return true;
}

function reloadMap(data){
	var getopts = data;
	var opts = $("#mapsearch").gMap.current;
	getopts.address = opts.address;
	getopts.latitude = opts.latitude;
	getopts.longitude = opts.longitude;
	getopts.zoom = opts.zoom;
	$("#mapsearch").gMap(getopts);
}