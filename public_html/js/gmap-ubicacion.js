$().ready(function(){
	
	$("#ubicacion").submit(function() {
		var opts = $("#mapsearch").gMap.current;
		opts.address = $("#ubicacion").val();
		opts.latitude = 0;
		opts.longitude = 0;
		$("#mapsearch").gMap(opts);
	});

	$("#ubicacion-button").click(function() {
		  $('#ubicacion').submit();
	});
});