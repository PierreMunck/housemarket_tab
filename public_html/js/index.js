$().ready(function(){
	
	$('#b-encuentra').mouseover(function() {
		$('#img-demo').removeClass("pantalla-central-profesionales pantalla-central-publica")
		$('#img-demo').addClass("pantalla-central-encuentra")
		return false;
	});
	
	$('#b-profesionales').mouseover(function() {
		$('#img-demo').removeClass("pantalla-central-encuentra pantalla-central-publica")
		$('#img-demo').addClass("pantalla-central-profesionales")
		return false;
	});
	
	$('#b-publica').mouseover(function() {
		$('#img-demo').removeClass("pantalla-central-profesionales pantalla-central-encuentra")
		$('#img-demo').addClass("pantalla-central-publica")
		return false;
	});
});