<?php

error_reporting(E_ALL);

// Define path to application directory
defined('PUBLIC_HTML_PATH') || define('PUBLIC_HTML_PATH', realpath(dirname(__FILE__). '/../public_html' ));
defined('APPLICATION_PATH') || define('APPLICATION_PATH', realpath(dirname(__FILE__) . '/../housemarkettab/application'));
/* Si no esta definida esta variable de entorno en los archivos de configuracion del servidor web, no se cargara la configuracion de desarrollo
 * y se cargará por defecto la configuracion de produccion, generando un redireccionamiento a hmapp.com
*/
// Define application environment.
defined('APPLICATION_ENV') || define('APPLICATION_ENV', (getenv('APPLICATION_ENV') ? getenv('APPLICATION_ENV') : 'production'));

// Ensure library/ is on include_path
set_include_path(implode(PATH_SEPARATOR, array(
		realpath(APPLICATION_PATH . '/../library/'),
		realpath(APPLICATION_PATH . '/models/'),
		realpath(APPLICATION_PATH . '/views/'),
		realpath(APPLICATION_PATH . '/controllers/'),
		get_include_path(),
)));

require_once 'Zend/Log.php';
require_once 'Zend/Log/Writer/Stream.php';
require_once 'Zend/Registry.php';

$logger = new Zend_Log();

$writer = new Zend_Log_Writer_Stream(APPLICATION_PATH . '/data/logs/app.log');
$logger->addWriter($writer);

Zend_Registry::set('log', $logger);

require_once 'Facebook/Facebook.php';

$pageId = '112757475509748';

$appfb = new Facebook_Facebook(array('appId' => '151516144978488', 'secret' => '5207a283291a3b0584263fddcc8c8157'));

if(isset($_GET['code'])){
	$appfb->safecheck($_GET['code']);
}

$uid = $appfb->getUser();

echo date('l jS \of F Y h:i:s A');

if(isset($uid) && $uid != 0){
	if($uid == 100000041940062){
		if(isset($_GET['token'])){
			savetoken($_GET['token']);
		}
		
		$accountsInfo = $appfb->api('/'.$uid .'/accounts');
		foreach($accountsInfo['data'] as $page){
			if($page['id'] == $pageId){
				savetoken($page['access_token']);
			}
		}
		echo '<html>';
		echo '<script>';
		echo 'setInterval(function(){window.top.location.reload()},900000);';
		echo '</script>';
		echo '<html>';
	}
}else{
	$url = 'https://www.facebook.com/dialog/oauth/?client_id=151516144978488&redirect_uri='. urlencode('http://tab.hmapp.com/recupToken.php');
	echo '<html>';
	echo '<script>';
	echo 'window.top.location = "'. $url .'"';
	echo '</script>';
	echo '<html>';
	die();
}

function savetoken($token){
	$fp = fopen(".FacebooktokenApp", "w");
	fwrite($fp, $token);
	fclose($fp);
}
?>