<?php 

require_once 'config.inc';

$sql =  'SELECT p.CodigoPropiedad, p.Uid ';
$sql .= 'FROM propropiedad p ';
$sql .= 'where PubOnPage = 0 ';
$sql .= 'and FechaPublica IS NOT NULL ';
$sql .= 'ORDER BY FechaPublica ASC '; 
$sql .= 'limit 1';

$PropriedadList = $db->fetchAll($sql);

$pageCfg = array(
		'123122201143956' => array(
				'name' => 'testhousemarket',
				'Id' => '123122201143956',
				'adminUser' => '100000041940062',
			),
		'112757475509748'  => array(
				'name' => 'housemarket',
				'Id' => '112757475509748',
				'adminUser' => '100000041940062',
			),
		'207668309275493'  => array(
				'name' => 'Discover Real Estate',
				'Id' => '207668309275493',
				'adminUser' => '512674512',
			),
	);

//page to be publish
$pageId = '112757475509748';

foreach ($PropriedadList as $Propriedad){
	$link = 'http://propiedad.hmapp.com/show/propiedad/'. $Propriedad['CodigoPropiedad'];
	
	//publicate on pagina principal
	$pubOnPage = pushLinkOnpage($pageCfg[$pageId],$link);
	if($pubOnPage == true){
		$update = 'UPDATE propropiedad set pubonpage = 1 where CodigoPropiedad ='. $Propriedad['CodigoPropiedad'];
		$db->query($update);
		print('propiedad '. $Propriedad['CodigoPropiedad'] . ' publish on '. $pageCfg[$pageId]['name'] ."\n");
	}else{
		print('propiedad '. $Propriedad['CodigoPropiedad'] . ' publish FAIL' ."\n" );
	}
	
	$sql = 'select pageid from fb_page where Uid ='. $Propriedad['Uid'];
	$PageList = $db->fetchAll($sql);
	foreach($PageList as $Page){
		if($Page['pageid'] == $pageId){
			continue;
		}
		if(!isset($pageCfg[$Page['pageid']])) {
			$pageCfg[$Page['pageid']] = array('Id' => $Page['pageid'], 'adminUser' => $Propriedad['Uid']);
		}
		$pubOnPage = pushLinkOnpage($pageCfg[$Page['pageid']],$link);
		print('propiedad '. $Propriedad['CodigoPropiedad'] . ' publish on '. $Page['pageid'] ."\n");
	}
	
	
}

function pushLinkOnpage($page,$link) {
	$appfb = Zend_Registry::get('appfb');
	try {
		//token
		$appfb->setPageAccessToken($page['adminUser'],$page['Id']);
		$graph = '/'. $page['Id'] .'/feed';
		$msgPage = array('link' => $link);
		$appfb->api($graph, 'POST', $msgPage);
		$appfb->setUserAccessToken();
		return true;
	} catch(FacebookApiException $e){
		
	}
	return false;
}
?>