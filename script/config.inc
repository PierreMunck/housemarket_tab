<?php

// Define path to application directory
defined('PUBLIC_HTML_PATH') || define('PUBLIC_HTML_PATH', realpath(dirname(__FILE__). '/../public_html' ));
defined('APPLICATION_PATH') || define('APPLICATION_PATH', realpath(dirname(__FILE__) . '/../housemarkettab/application'));
/* Si no esta definida esta variable de entorno en los archivos de configuracion del servidor web, no se cargara la configuracion de desarrollo
 * y se cargará por defecto la configuracion de produccion, generando un redireccionamiento a hmapp.com
*/
// Define application environment.
defined('APPLICATION_ENV') || define('APPLICATION_ENV', (getenv('APPLICATION_ENV') ? getenv('APPLICATION_ENV') : 'production'));

// Ensure library/ is on include_path
set_include_path(implode(PATH_SEPARATOR, array(
		realpath(APPLICATION_PATH . '/../library/'),
		realpath(APPLICATION_PATH . '/models/'),
		realpath(APPLICATION_PATH . '/views/'),
		realpath(APPLICATION_PATH . '/controllers/'),
		get_include_path(),
)));

/*
 * LOGGER
 */

require_once 'Zend/Log.php';
require_once 'Zend/Log/Writer/Stream.php';
require_once 'Zend/Registry.php';

$logger = new Zend_Log();

$writer = new Zend_Log_Writer_Stream(APPLICATION_PATH . '/data/logs/app.log');
$logger->addWriter($writer);

Zend_Registry::set('log', $logger);
/*
 * DataBase
 */
require_once 'Zend/Db.php';

try {
	// Seteos para la conexi�n con la base de datos
	$db = Zend_Db::factory('MYSQLI', array(
			'host'     => 'localhost',
			'username' => 'hmapp',
			'password' => 'H1M2A3p4p_',
			'dbname'   => 'hmapp'
	));

	//Test de conexi�n con la base de datos
	$db->getConnection();

	Zend_Registry::set('db', $db);
} catch (Zend_Db_Adapter_Exception $e) {
	//Sucedi� un error con las credenciales del usuario o la base de datos.
	die($e->getMessage());
} catch (Zend_Exception $e) {
	// Sucedi� un error inexperado
	die($e->getMessage());
}

require_once 'Facebook/Facebook.php';
$appfb = new Facebook_Facebook(array('appId' => '151516144978488', 'secret' => '5207a283291a3b0584263fddcc8c8157'));

$fp = fopen(PUBLIC_HTML_PATH ."/.FacebooktokenApp", "r");
$token = fread($fp, 8192);
fclose($fp);

$appfb->setAccessToken($token);

Zend_Registry::set('appfb', $appfb);
?>