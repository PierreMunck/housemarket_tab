<?php
/**
 * Zend Framework
 *
 * LICENSE
 *
 * This source file is subject to the new BSD license that is bundled
 * with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://framework.zend.com/license/new-bsd
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@zend.com so we can send you a copy immediately.
 *
 * @category   Zend
 * @package    Zend_Payment
 * @subpackage Gateway
 * @copyright  Copyright (c) 2005-2008 Zend Technologies USA Inc. (http://www.zend.com)
 * @license    http://framework.zend.com/license/new-bsd     New BSD License
 * @version    $Id:  $
 */
 
/**
 * @see Zend_Loader
 */
require_once 'Zend/Loader.php';
 
 
/**
 * Class for online Ecommerce charging/payments
 *
 * @category   Zend
 * @package    Zend_Payment
 * @subpackage Gateway
 * @copyright  Copyright (c) 2005-2008 Zend Technologies USA Inc. (http://www.zend.com)
 * @license    http://framework.zend.com/license/new-bsd     New BSD License
 * @version    $Id:  $
 */
abstract class Zend_Payment_Gateway_Abstract
{
     /**
     * Array with all options, each gateway can have own additional options
     *
     * @var array
     */
    protected $_fields = array();
     
    /**
     * Required options per gateways, things such as suplier, merchant account id
     * email addresess
     *
     * Other:
     *       'gatewayNamespace' => custom namespace for a gateway
     *
     * @var array
     */
    protected $_options = array(
         
        'gatewayNamespace' => null
     
    );
     
    /**
     * array containing reposnse from the gateway provider
     *
     * @var array
     */
    protected $_response = array();
     
    /**
     * Error code holder returned by the gateway provider
     *
     * @var int
     */
    protected $_errorcode = null;
     
     
    /**
     * Error string returned by the gateway provider
     *
     * @var string
     */
    protected $_error_string = null;
     
 
    /**
     * Constructor.
     *
     * $options is an array of key/value pairs or an instance of Zend_Config
     * containing configuration options.
     *
     * @param  array|Zend_Config $options An array or instance of Zend_Config having configuration data
     * @throws Zend_Payment_Exception
     */
    public function __construct($options)
    {
        /*
         * Verify that gateway parameters are in an array.
         */
        if (!is_array($options)) {
            /*
             * Convert Zend_Config argument to a plain array.
             */
            if ($options instanceof Zend_Config) {
                $options = $options->toArray();
            } else {
                /**
                 * @see Zend_Payment_Exception
                 */
                require_once 'Zend/Payment/Exception.php';
                throw new Zend_Payment_Exception('Gateway parameters must be in an array or a Zend_Config object');
            }
        }
         
        # Pass in the config to the array holder
        $this->_options = $options;
 
        $this->_checkRequiredOptions($options);
 
    }
     
    /**
     * Get a field from the fields array
     *
     * @param string|boolean|array|int $name
     */
    protected function getField($name)
    {
        return array_key_exists($name, $this->_fields) ? $this->_fields[$name] : null;
    }
     
    /**
     * Add a key=>value pair to the fields stack
     *
     * @param string $name
     * @param string|boolean|array|int $value
     * @return object
     */
    protected function setField($name, $value)
    {
        $this->_fields[$name] = $value;
        return $this;
    }
     
    /**
     * Magic method to set a field
     *
     * @param string $name
     * @param string|boolean|array|int $value
     * @return $value
     */
    protected function __set($name, $value)
    {
        return $this->setField($name, $value);
    }
     
    /**
     * Magic method to get a value of a fied by it's name
     *
     * @param  string $name
     */
    protected function __get($name)
    {
        return $this->getField($name);
    }
 
    /**
     * Returns the configuration variables in this gateway.
     * Or a single config var if the $value passed
     *
     * @param string $value
     * @return array|string on success exception on failure
     */
    public function getConfig($value=null)
    {
        if($value)
        {
            if(array_key_exists($value, $this->_options))
            {
                return $this->_options[$value];
            }
             
            throw new Zend_Payment_Gateway_Exception("The value {$value} was not found in the class config array");
        }
         
        return $this->_options;
    }
 
    /**
     * Was there any errors returned?
     *
     * @return boolean
     */
    protected function _hasError()
    {
        return $this->_error_string ? true : false;     
    }
     
    /**
     * Return the error string if any
     *
     * @return string
     */
    protected function _getError()
    {
        return $this->_error_string;
    }
     
    /**
     * returns ture/false if there is any response from
     * the gateway provider
     *
     * @return boolean
     */
    protected function _hasResponse()
    {
        return $this->_response ? true : false;
    }
     
    /**
     * Return the response array
     *
     * @return array
     */
    protected function _getResponse()
    {
        return $this->_response;
    }
     
    /**
     * Return the array of fields
     *
     * @return array
     */
    protected function _getFields()
    {
        return $this->_fields;
    }
     
     
    /**
     * Abstract Methods
     */
     
    /**
     * Do process
     *
     * @throws Zend_Payment_Gateway_Exception
     */
    abstract function process();
}