<?php

/**
 * Controller por defecto
 */
class Hm_System_Message extends Hm_Db_Table {
	/**
	 * Nombre de la tabla
	 * @var String
	 */
	protected $_name = 'HMSystemMessage';
	
	protected $_primary = 'CodigoMessage';
	
	protected $mode = null; // session or db
	
	
	public $CodigoMessage = null;
	public $Level = null;
	public $Status = null;
	public $Class = null;
	public $Message = null;
	public $Uid = null;
	
	
	public function TableDescribe(){
		return array(
				'CodigoMessage' => 'int',
				'Level' => 'string',
				'Status' => 'int',
				'Class' => 'string',
				'Message' => 'string',
				'Uid' => 'int',
		);
	}
	
	function __construct($mode = null) {
		parent::__construct();
		$this->mode = 'Db';
		if($mode){
			$this->mode = $mode;
		}
	}
	
	public function set($message = null ,$level = "info",$class = null){
		if ($message) {
			$this->Level = $level;
			if(is_string($message)){
				$this->Message = $message;
			} else {
				$this->Message = print_r($message,true);
			}
			$this->Status = 1;
			$this->save();
		}
	}
	
	public function Deliver(){
		return $this->__toString();
	}
	
	public function __toString(){
		$return = $this->search(array('Status' => 1));
		
		$output = '';
		if($return){
			$output .= "\n";
			foreach($return as $message){
				$MessageIds[] = $message['CodigoMessage'];
				$output .= '<div class='. $message['Level'] .'>';
				$output .= '<span>'. $message['Message'] .'</span>';
				$output .= '</div>';
				$output .= "\n";
			}
			if(!empty($MessageIds)){
				$this->PushStatusDeliver($MessageIds);
			}
		}
		return $output;
	}
	
	public function PushStatusDeliver($MessageIds){
		$data = array('Status' => 0);
		$ids = implode(',', $MessageIds);
		$this->update($data, "CodigoMessage in ($ids)");
	}
	
	/**
	 * carga el objecto con el resltado de una seleccion
	 * @param unknown_type $paramField
	 */
	public function search($paramField = null) {
	
		if(!isset($paramField) && !is_array($paramField)){
			return false;
		}
	
		/*
		if(!$this->checkValuequery($paramField,$this)){
			return false;
		}
		*/
		
		foreach($paramField as $fieldKey => $fieldVal){
			$where = ' '. $fieldKey .' = ? ';
			$whereparam[] = $fieldVal;
		}
	
		$select = $this->select();
		$select->setIntegrityCheck(false);
		$select->from($this->getTableName());
	
		// select by field
		foreach($paramField as $fieldKey => $fieldVal){
			$select->where(' '. $fieldKey .' = ? ', $fieldVal);
		}
	
		//depend table
		if(is_array($this->getDependentTables())){
			foreach($this->getDependentTables() as $depTable => $depRules){
				if($depRules['type'] == 'left'){
					$select->joinLeft($depTable, $depRules['value']);
				}
				if($depRules['type'] == 'strict'){
					$select->join($depTable, $depRules['value']);
				}
			}
		}
	
		$rs = $this->fetchAll($select);
		$result = $rs->toArray();
		if(is_array($result) && isset($result[0])){
			 return $result;
		}
	}
	
}