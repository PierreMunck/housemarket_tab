<?php 

class Hm_Facebook_Message {
	
	public $body = null;
	public $picture = null;
	public $link = null;
	public $name = null;
	public $caption = 'housemarket';
	public $description = null;
	
	public function __construct($message = null) {
		if(is_string($message)){
			$this->body = $message;
		}
	}
	
	public function toarray(){
		$return = array('cb' => '');
		if(isset($this->body)){
			$return['message']=$this->body;
		}
		if(isset($this->picture)){
			$return['picture']=$this->picture;
		}
		if(isset($this->link)){
			$return['link']=$this->link;
		}
		if(isset($this->name)){
			$return['name']=$this->name;
		}
		if(isset($this->caption)){
			$return['caption']=$this->caption;
		}
		if(isset($this->description)){
			$return['description']=$this->description;
		}
		return $return;
	}
}
?>
