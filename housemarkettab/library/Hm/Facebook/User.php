<?php

/**
 * Clase para inicializar aplicacion para
 * acceder a la informacion de Facebook.
 * Es un sigleton y se debe usar con cache
 *
 * @author Jeronimo Martinez Sanchez.
 * Modified: Irving Medina - SolInvestment
 */

class Hm_Facebook_User {

	// Contenedor de la instancia del singleton
	private static $instance;
	
    public $conf;
    public $facebook;
    public $log;
    
    // friend ID
    public $friendsIDs;
    private $friends_ids_raw;
    
    // permision Face book
    private $fbPerms = null;
    
    // friends application
    private $fbFriendsApp = null;
    
    // facebook User pages
    private $fbPagesUser = null;
    
    // facebook User data
    private $fbDataUser = null;
    
    // facebook User Granted Pages
	private $fbUserGrantedPermissions = null;
	
	// facebook User Información
	private $fbUserInformation = null;
	
	// facebook MutualFriends
	private $fbMutualFriends = null;
	
    public static function getInstance() {
    	if (!isset(self::$instance)) {
    		$c = __CLASS__;
    		self::$instance = new $c;
    	}
    
    	return self::$instance;
    }
    
    public function __construct() {
        $this->facebook = Zend_Registry::get('Facebook_App');
        $this->conf = Zend_Registry::get('config');
        if(Zend_Registry::isRegistered('log')){
        	$this->log = Zend_Registry::get('log');
        }
    }

    // Evita que el objeto se pueda clonar
    public function __clone() {
    	trigger_error('Clone is not allowed.', E_USER_ERROR);
    }
    
    public function get_uid() {
        return $this->uid;
    }
    

    public function getFriendsApp() {
    	if(!isset($this->fbFriendsApp)){
	    	try {
	    		$fql = 'SELECT uid, first_name, last_name, pic_small, pic_square, profile_url FROM user
	    		WHERE uid IN (SELECT uid2 FROM friend WHERE uid1="' . $this->uid . '") AND is_app_user ';
	    		$this->fbFriendsApp = $this->facebook->api(array('method' => 'fql.query','query' => $fql));
	    	} catch (FacebookApiException $e) {
	    		Zend_Session::destroy();
	    		$URL = $_SERVER['HTTP_REFERER'];
	    		header ("Location: $URL");
	    		$this->log->err( __FILE__ .' '. __LINE__ .' '. print_r($e->getResult(),true));
	    		die(); 
	    	}
    	}
    	
    	return array_slice($this->fbFriendsApp,0 , 8);
    }
    
    public function getFriendsAppAll() {
    	if(!isset($this->fbFriendsApp)){
	    	try {
	    		$fql = 'SELECT uid, first_name, last_name, pic_small, pic_square, profile_url FROM user 
	    		WHERE uid IN (SELECT uid2 FROM friend WHERE uid1="' . $this->uid . '") AND is_app_user ';
	    		$this->fbFriendsApp = $this->facebook->api(array('method' => 'fql.query','query' => $fql));
	    	} catch (FacebookApiException $e) {
	    		Zend_Session::destroy();
	    		$URL = $_SERVER['HTTP_REFERER'];
	    		header ("Location: $URL");
	    		$this->log->err( __FILE__ .' '. __LINE__ .' '. print_r($e->getResult(),true));
	    		die();
	    	}
    	}

    	return $this->fbFriendsApp;
    }

    public function getFriendsAll() {
    		try {
    			$fql = 'SELECT uid, first_name, last_name, pic_small, pic_square, profile_url FROM user 
	    		WHERE uid IN (SELECT uid2 FROM friend WHERE uid1="' . $this->uid . '") ';
    			$listFriends = $this->facebook->api(array('method' => 'fql.query','query' => $fql));
    		} catch (FacebookApiException $e) {
    			
    			$this->log->err( __FILE__ .' '. __LINE__ .' '. print_r($e,true));
    			$this->log->err( __FILE__ .' '. __LINE__ .' '. print_r($e->getResult(),true));
    			die();
    		}
    
    	return $listFriends;
    }
    
    public function getFanpages() {
    		try {
    			$fql = 'SELECT page_id FROM page_fan WHERE uid="' . $this->uid . '"';
    			$Fanpages = $this->facebook->api(array('method' => 'fql.query','query' => $fql));
    		} catch (FacebookApiException $e) {
    
    			$this->log->err( __FILE__ .' '. __LINE__ .' '. print_r($e,true));
    			$this->log->err( __FILE__ .' '. __LINE__ .' '. print_r($e->getResult(),true));
    			die();
    		}
    
    	return $Fanpages;
    }
    
    public function GetTotalMutualFriends($uid) {
        $mutualFriends = $this->GetMutualFriends($uid);
        if (is_array($mutualFriends))
            return count($mutualFriends);
        else
            return 0;
    }

    public function GetMutualFriends($target_uid, $limit=' LIMIT 6') {
    	if(!isset($this->fbMutualFriends)){
	        $uid=Zend_Registry::get('uid');
	        $uids="SELECT uid2 FROM friend where uid1=".$uid." and uid2 IN (SELECT uid1 FROM friend WHERE uid2=".$uid.") ORDER BY rand()"; 
	        $fql=sprintf('SELECT uid, first_name, last_name, pic_square, profile_url FROM user WHERE uid IN (%s) %s',$uids,$limit);
	        $this->fbMutualFriends = $this->execFql($fql);
    	}
        return $this->fbMutualFriends;
    }
   
    public function getNamebyId($uid) {
    	if(!isset($this->fbNamebyId[$uid])){
	        try {
	            $fql='SELECT name FROM user WHERE uid IN ("' . $uid . '") AND is_app_user ';
	            $this->fbNamebyId[$uid] = $this->facebook->api(array('method' => 'fql.query','query' =>$fql));            
	        } catch (Exception $e) {
	            Zend_Session::destroy();
	    		$URL = $_SERVER['HTTP_REFERER'];
	    		header ("Location: $URL");
	    		$this->log->err( __FILE__ .' '. __LINE__ .' '. print_r($e->getResult(),true));
	    		die();
	        }
    	}
        return $this->fbNamebyId[$uid];
    }

    private function loadFriendsIDsArray() {
        //prepare an array that contains the IDs of all this user's friends
        if(!isset($this->friends_ids_raw)){
	    	try {
	    		$fql = "SELECT uid2 FROM friend WHERE uid1=" . $this->uid;
	        	$this->friends_ids_raw = $this->facebook->api(array('method' => 'fql.query','query' =>$fql));
	    	} catch (FacebookApiException $e) {
	    		Zend_Session::destroy();
	    		$URL = $_SERVER['HTTP_REFERER'];
	    		header ("Location: $URL");
	    		$this->log->err( __FILE__ .' '. __LINE__ .' '. print_r($e->getResult(),true));
	    		die();
	    	}
        }
        
        $counter = 0;
        foreach ($this->friends_ids_raw as $var => $value) { //take out of associative array and put into a simpler array format
            $this->friendsIDs[$counter] = $value['uid2'];
            $counter++;
        }
    }

    public function get_friendsIDs($refresh=false) {
        //make the array if it's not already set, otherwise, return it

        if ((count($this->friendsIDs) < 1) || ($refresh)) {
            $this->loadFriendsIDsArray();
        }
        return ($this->friendsIDs);
    }
    
    public function getDataUser($uid,$fields = array('uid','first_name','last_name','name','website','email','profile_url','pic_small','pic_big')) {
    	$log = Zend_Registry::get('log');
    	$log->info( __CLASS__ . ' ' . __METHOD__ . ' (' . $uid . ')'  );
    	$log->info( print_r($fields,true)  );
    	$getfromfb = false;
    	
    	$profil = new Hm_Profil();
    	$profil->search(array('uid' => $uid));
    	
    	if(isset($profil) && isset($profil->data)){
    		$this->fbDataUser[$uid] = json_decode($profil->data);
    	}
    	
    	if(!isset($this->fbDataUser[$uid]) || !is_array($this->fbDataUser[$uid])){
    		$getfromfb = true;
    	}else{
    		foreach ($fields as $fname){
    			if(!isset($this->fbDataUser[$uid][$fname])){
    				$getfromfb = true;
    			}
    		}
    	}
    	if($getfromfb){
    		try {
    			$result = $this->facebook->api("/". $uid);
    			// get info
    			foreach ($result as $fname => $fvalue){
    				if($fname == 'id'){
    					$this->fbDataUser[$uid]['uid'] = $fvalue;
    				}
    				$this->fbDataUser[$uid][$fname] = $fvalue;
    			}
    			// get specific info
    			$fieldsreq=join(",",$fields);
    			$fql=sprintf("SELECT+%s+FROM+user+WHERE+uid=%s",$fieldsreq,$uid);
    			$result = $this->facebook->api('/fql?q='.$fql);
    			if(isset($result['data'][0]) && is_array($result['data'][0])){
    				foreach ($result['data'][0] as $fname => $fvalue){
    					$this->fbDataUser[$uid][$fname] = $fvalue;
    				}
    			}
    			
    			if(isset($profil)){
    				$profil->uid = $uid;
    				$profil->data = json_encode($this->fbDataUser[$uid]);
    				$profil->expire = time() + 86400;
    			}else{
    				$data = array();
    				$data['uid'] = $uid;
    				$data['data'] = json_encode($this->fbDataUser[$uid]);
    				$data['expire'] = time() + 86400;
    				$profil = new Hm_Profil($data);
    			}
    			$profil->save();
    			
    		} catch (FacebookApiException $e) {
    			Zend_Session::destroy();
    			if(isset($_SERVER['HTTP_REFERER'])){
	    			$URL = $_SERVER['HTTP_REFERER'];
    			}else{
    				$cfg = Zend_Registry::get('config');
    				$URL = $cfg['local_app_url'];
    			}
	    		//header ("Location: $URL");
	    		$this->log->err( __FILE__ .' '. __LINE__ .' '. print_r($e->getResult(),true));
	    		die();
    		}
    	}
    	$log->info( "return" . print_r($this->fbDataUser[$uid],true)  );
    	return $this->fbDataUser[$uid];
    }
    
    public function getUserInformation() {
    	if(!isset($this->fbUserInformation)){
	    	try {
	    		$this->fbUserInformation = $this->facebook->api('/me');
	    	} catch (FacebookApiException $e) {
	    		Zend_Session::destroy();
	    		$URL = $_SERVER['HTTP_REFERER'];
	    		header ("Location: $URL");
	    		$this->log->err( __FILE__ .' '. __LINE__ .' '. print_r($e->getResult(),true));
	    		die();
	    	}
    	}
     	return $this->fbUserInformation;
    }
     
     public function getUserGrantedPermissions($uid, array $permissions) {
     	if(!isset($this->fbUserGrantedPermissions)){
        	$fql = 'select ' . implode(', ', $permissions) . ' from permissions where uid = ' . $uid;
        	$this->fbUserGrantedPermissions = $this->execFql($fql);
     	}
        return $this->fbUserGrantedPermissions;
     }
     
     /**
      * get the user page from the facebook Api
      * @param unknown_type $uid
      * @param unknown_type $asTable
      */
     public function getPagesUser($uid ,$asTable = true) {

     	if(!isset($this->fbPagesUser)){
	        $fql = "SELECT page_id, name, has_added_app, pic_big, pic_small, fan_count FROM page WHERE page_id IN (SELECT page_id FROM page_admin WHERE uid = " . $uid . ")";        
	        $pages = $this->execFql($fql);
	                
	        if($asTable){
	            $this->fbPagesUser = new Table($pages);
	        }
     	}
        
        return $this->fbPagesUser;
     }
     
     /**
      * post a message to the user wall
      * @param unknown_type $message
      * @param unknown_type $to
      */
     public function doPost($message,$to='me'){
         if (isset($message)){
            try {
                $towall='/'.$to.'/feed';
                $post=array('cb' => '');
                if(isset($message->body)){
                    $post['message']=$message->body;
                }
                if(isset($message->picture)){
                    $post['picture']=$message->picture;
                }
                if(isset($message->link)){
                    $post['link']=$message->link;
                }
                if(isset($message->name)){
                    $post['name']=$message->name;
                }
                if(isset($message->caption)){
                    $post['caption']=$message->caption;
                }
                if(isset($message->description)){
                    $post['description']=$message->description;
                }
                $statusUpdate = $this->facebook->api($towall, 'post', $post);
            } catch (FacebookApiException $e) {
                Zend_Session::destroy();
	    		$URL = $_SERVER['HTTP_REFERER'];
	    		header ("Location: $URL");
	    		$this->log->err( __FILE__ .' '. __LINE__ .' '. print_r($e->getResult(),true));
	    		die();
            }
        }
     }
     
     /**
      * execute a query to facebook Api
      * @param unknown_type $fql
      * @return unknown
      */
     public function execFql($fql) {
     	$store = null;
     	try {
     		$store = $this->facebook->api(array('method' => 'fql.query','query' =>$fql,'callback'=>''));
     	} catch (FacebookApiException $e) {
     		Zend_Session::destroy();
	    	$URL = $_SERVER['HTTP_REFERER'];
	    	header ("Location: $URL");
	    	$this->log->err( __FILE__ .' '. __LINE__ .' '. print_r($e->getResult(),true));
	    	die();
     	}
        return $store;
     }
     
     /**
      * execute a multiquery to facebook Api
      * @param unknown_type $multiQuery
      * @return unknown
      */
     public function execMultiqueryFql($multiQuery) {
     	try {
     		$store = $this->facebook->api(array('method' => 'fql.multiquery', 'queries' => $multiQuery, 'callback' => ''));
     	} catch (FacebookApiException $e) {
			Zend_Session::destroy();
	    	$URL = $_SERVER['HTTP_REFERER'];
	    	header ("Location: $URL");
	    	$this->log->err( __FILE__ .' '. __LINE__ .' '. print_r($e->getResult(),true));
	    	die();
     	}

        return $store;
     }
     
     public function hasPermission($perms='',$fb='') {
     	if(!isset($this->fbPerms)){
	        $uid = Zend_Registry::get('uid');        
	        if(!empty($fb)){
	            $uid = $fb;
	        }
	        $this->fbPerms = $this->execFql("SELECT ".$perms." FROM permissions WHERE uid=".$uid); 
     	}       
        
        return (isset($this->fbPerms[0]))?in_array(0, $this->fbPerms[0]):false;
     }
     
}

?>