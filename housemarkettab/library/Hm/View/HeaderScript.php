<?php
class Hm_View_HeaderScript{
   private $headerScript = array();
   private $ScriptNumber = 0;
   
   public function setScript($Script, $name = null){
   	if($name == null){
   		$this->ScriptNumber++;
   		$name = 'Script_'.$this->ScriptNumber;
   	}
   	$this->headerScript[$name] = $Script;
   }
   
   public function __toString() {
   	$output = '';
   	$output .= "\n";
   	foreach($this->headerScript as $key => $value){
   		$output .= "<script type=\"text/javascript\">\n";
   		$output .= $value;
   		$output .= "\n</script>";
   	}
   	$output .= "\n";
   	return $output;
   }
   
}