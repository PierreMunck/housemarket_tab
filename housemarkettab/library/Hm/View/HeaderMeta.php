<?php
class Hm_View_HeaderMeta{
   private $headerOg = array();
   private $headerMeta = array();
   private $headerLink = array();
   
   public function setProperty($property_name, $property_value){
   	$this->headerOg[$property_name] = $property_value;
   }

   public function setLink($property_name, $property_value){
   	$this->headerLink[$property_name] = $property_value;
   }
   
   public function setName($name_name, $name_value){
   	$this->headerMeta[$name_name] = $name_value;
   }
   
   public function __toString() {
   	$output = '';
   	$output .= "\n";
   	// No functiona con el share no se porque
   	/*foreach($this->headerOg as $key => $value){
   		$output .= '<meta property="'. $key .'" content="'. $value .'"/>';
   		$output .= "\n";
   	}*/
   	$output .= "\n";
   	foreach($this->headerMeta as $key => $value){
   		$output .= '<meta name="'. $key .'" content="'. $value .'"/>';
   		$output .= "\n";
   	}
   	$output .= "\n";
   	foreach($this->headerLink as $key => $value){
   		$output .= '<link rel="'. $key .'" href="'. $value .'"/>';
   		$output .= "\n";
   	}
   	return $output;
   }
   
}