<?php 

class Hm_Form_ContactarHousemarket extends Hm_Form{
	
	private $signed_request = null;
	
	protected $formDesc = array(
			'Nombre' => array(
					'type' => 'Text',
					'required' => true,
					'Label' => 'Nombre',
					'Labelinner' => true,
			),
			'Email' => array(
					'type' => 'Email',
					'required' => true,
					'Label' => 'Email',
					'Labelinner' => true,
			),
			'Telefono' => array(
					'type' => 'Text',
					'Label' => 'Telefono',
					'Labelinner' => true,
			),
			'Texto' => array(
					'type' => 'TextArea',
					'required' => true,
					'Label' => 'Texto...',
					'Labelinner' => true,
			),
			'Label' => 'Contactarnos',
			'submitName' => 'contactar',
			'submitLabel'=> 'Contactar',
			//'MessagePosition' => 'bottom',
			'#action' => '',
		);
	
	public function __construct($signed, $formState = null , $params = null){
		
		if(isset($_POST) && !empty($_POST)){
			$formState = array_merge($_POST,$formState);
			if(isset($formState['form-contact-housemarket'])){
				$this->submited = true;
			}
		}
		parent::__construct('form-contact-housemarket',$formState,$params);
		
	}
	
	public function validate(){
		return parent::validate();
	}
	
	public function submit(){
		//contact
		$signed = Zend_Registry::get('Facebook_Signed');
		$cfg = Zend_Registry::get('config');
		
		$view = new Zend_View();
		$view->setScriptPath($cfg['resources']['view']['scriptPath']['default']);
		
		$partialparams = array('formvalue' => $this->formState['values']);
		
		
		$senderEmail = $this->formState['values']['Email'];
		
		// No mail to the sender
		if(isset($senderEmail)){
			$senderName = $this->formState['values']['Nombre'];
			$mail = new Zend_Mail();
			$body = $view->partial('mail/contact-housemarket.phtml',$partialparams);
			$mail->setBodyHtml($body);
			$body = $view->partial('mail/contact-housemarket-text.phtml',$partialparams);
			$mail->setBodyText($body);
			$mail->setFrom($senderEmail, $senderName);
			$mail->addTo('info@hmapp.com', 'Housemarket');
			$mail->addBcc('eduardo.somarriba@gmail.com', 'Eduardo Sommariba');
			$mail->addBcc('pierre.munck@gmail.com', 'Pierre Munck');
			$mail->setSubject('[Housemarket Tab] pregunta de '. $senderName);
			$mail->send();
		}
		
		$this->formDesc['Message'] = "Su mensaje ha sido enviado";
	}
}

?>