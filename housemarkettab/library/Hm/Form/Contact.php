<?php 

class Hm_Form_Contact extends Hm_Form{
	
	private $signed_request = null;
	
	protected $formDesc = array(
			'BrokerId' => array(
					'type' => 'Hidden',
					'required' => true,
					'value' => '',
			),
			'PropiedadId' => array(
					'type' => 'Hidden',
					'required' => true,
					'value' => '',
			),
			'Nombre' => array(
					'type' => 'Text',
					'required' => true,
					'Label' => 'Nombre',
					'Labelinner' => true,
			),
			'Email' => array(
					'type' => 'Email',
					'required' => true,
					'Label' => 'Email',
					'Labelinner' => true,
			),
			'Telefono' => array(
					'type' => 'Text',
					'Label' => 'Telefono',
					'Labelinner' => true,
			),
			'Texto' => array(
					'type' => 'TextArea',
					'required' => true,
					'Label' => 'Texto...',
					'Labelinner' => true,
			),
			'Label' => 'Formulario',
			'submitName' => 'contactar',
			'submitLabel'=> 'Contactar',
			'MessagePosition' => 'bottom',
			'#action' => '',
		);
	
	public function __construct($signed, $formState = null , $params = null){
		if(isset($_POST) && !empty($_POST)){
			$formState = array_merge($_POST,$formState);
			if(isset($formState['form-contact'])){
				$this->submited = true;
			}
		}
		parent::__construct('form-contact',$formState,$params);
		
	}
	
	public function validate(){
		return parent::validate();
	}
	
	public function submit(){
		//contact
		$signed = Zend_Registry::get('Facebook_Signed');
		$cfg = Zend_Registry::get('config');
		/*$uid = $signed->get('user_id');
		if(!$uid){
			return false;
		}
		$signed->getUserInfo();*/
		
		$propId = $this->formState['values']['PropiedadId'];
		$propiedad = new Hm_Pro_Propiedad();
		$searchparam = array('CodigoPropiedad' => $propId);
		$propiedad->search($searchparam);
		
		$brokerId = $this->formState['values']['BrokerId'];
		$broker = new Hm_Cli_Cliente();
		$broker->search(array('Uid' => $brokerId));
		
		$view = new Zend_View();
		$view->setScriptPath($cfg['resources']['view']['scriptPath']['default']);
		
		//create mail
		$mail = new Zend_Mail();
		
		$partialparams = array('Propiedad' => $propiedad, 'Broker' => $broker, 'Signed' => $signed->getArray(), 'formvalue' => $this->formState['values']);
		
		
		$senderEmail = $this->formState['values']['Email'];
		// No mail to the sender
		/*if(isset($senderEmail)){
			$mail = new Zend_Mail();
			$body = $view->partial('mail/contact-sender.phtml',$partialparams);
			$mail->setBodyHtml($body);
			$body = $view->partial('mail/contact-sender-text.phtml',$partialparams);
			$mail->setBodyText($body);
			$mail->setFrom('contact@hmapp.com', 'Housemarket App');
			$mail->addTo($senderEmail, $this->formState['values']['Nombre']);
			$mail->setSubject('[Housemarket] You have contact '. $broker->NombreCliente);
			$mail->send();
		}*/
		
		
		// verificamos que el vendedor no hace parte de un grupo
		$pagelist =  new Hm_Fb_PageList();
		$pagelist->search(array('uid' => $propiedad->Uid));
		foreach($pagelist as $pagebroker){
			$page =  new Hm_Cli_Page();
			$page->search(array('pageid' => $pagebroker->pageid));
			if(isset($page->email) && isset($this->formState['values']['Email'])){
				$page->NombreCliente = $page->nombre;
				$partialparams['Broker'] = $page;
				$mail = new Zend_Mail();
				$body = $view->partial('mail/contact-broker.phtml',$partialparams);
				$mail->setBodyHtml($body);
				$body = $view->partial('mail/contact-broker-text.phtml',$partialparams);
				$mail->setBodyText($body);
				$mail->setFrom($senderEmail, $this->formState['values']['Nombre']);
				$mail->addTo($page->email, $page->nombre);
				$mail->addBcc('pierre.munck@gmail.com', 'Pierre Munck');
				$mail->addBcc('eduardo.somarriba@gmail.com', 'Eduardo Sommariba');
				$mail->setSubject('[Housemarket]'. $page->nombre .' Alguien ha mostrado inter�s en tu propiedad' );
				$mail->send();
			}
		}
		
		if(isset($broker->EMail) && isset($this->formState['values']['Email'])){
			$partialparams['Broker'] = $broker;
			$mail = new Zend_Mail();
			$body = $view->partial('mail/contact-broker.phtml',$partialparams);
			$mail->setBodyHtml($body);
			$body = $view->partial('mail/contact-broker-text.phtml',$partialparams);
			$mail->setBodyText($body);
			$mail->setFrom($senderEmail, $this->formState['values']['Nombre']);
			$mail->addTo($broker->EMail, $broker->NombreCliente);
			$mail->addBcc('pierre.munck@gmail.com', 'Pierre Munck');
			$mail->addBcc('eduardo.somarriba@gmail.com', 'Eduardo Sommariba');
			$mail->setSubject('[Housemarket]'. $broker->NombreCliente .' Alguien ha mostrado inter�s en tu propiedad' );
			$mail->send();
		}
		
		$this->formDesc['Message'] = "Su mensaje ha sido enviado";
	}
}

?>