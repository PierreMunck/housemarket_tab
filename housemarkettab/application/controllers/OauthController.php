<?php

/**
 * Error Controller
 */
class OauthController extends Zend_Controller_Action {

	protected $cfg;
	
    /**
     * Antes de procesar alguna accion
     */
    public function preDispatch() {
        
    }
    
    /**
     * Buscar en el mapa
     */
    public function authorizeAction() {
    	$params = $this->_request->getParams();
    	$this->cfg = Zend_Registry::get('config');
    	$this->view->facebook_appid = $this->cfg['facebook_appid'];
    	$this->view->facebook_perms = $this->cfg['facebook_perms'];
    	$this->view->local_app_url = $this->cfg['local_app_url'];
    	
    	if(isset($params['old_param'])){
    		// change permition por los permiso especifico de la pagina
    		if(isset($params['signed_request'])){
    			$signed_request = $params['signed_request'];
    			$rules = $signed_request->getAccessRule($params['old_param']);
    			if(is_array($rules)){
    				$this->view->facebook_perms = implode(',',$rules);
    			}
    		}
    		
    		// change the redirect por las paginas llamadas
    		$this->view->local_app_url .= $params['old_param']['controller'] ."/". $params['old_param']['action'];    		
    		unset($params['old_param']['controller']);
    		unset($params['old_param']['action']);
    		unset($params['old_param']['module']);
    		unset($params['old_param']['code']);
    		unset($params['old_param']['signed_request']);
    		unset($params['old_param']['scope']);
    		$old_param = array();
    		foreach ($params['old_param'] as $key => $value){
    			$old_param[] = $key ."=". $value;
    		}
    		if(!empty($old_param)){
    			$this->view->local_app_url .= "?". implode('&',$old_param);
    		}
    		
    	}
    	$this->view->fb_app_url = $this->cfg['fb_app_url'];
    }
    
    

}

?>