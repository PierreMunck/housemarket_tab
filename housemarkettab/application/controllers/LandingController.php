<?php

class LandingController extends Hm_MainController {
	
	public function indexAction(){
		$this->appendStylesheet('/css/landing.css');
		$this->appendScriptFile('/js/jquery/jquery-1.3.2.js');
		$this->appendScriptFile('/js/landing-presentacion.js');
		
		$this->view->urlInstall = $this->getInstallUrl();
	}
	
	public function propiedadAction() {
		$this->view->url = 'https://propiedad.hmapp.com/show/propiedad/'. $_GET['propiedad_id'];
		$script = 'window.top.location ='.'\'https://propiedad.hmapp.com/show/propiedad/'. $_GET['propiedad_id'] .'\'';
		//$this->view->headerScript->setScript($script,'redirect_propiedad');
	}
	
	public function contactAction(){
		$this->appendStylesheet('/css/landing.css');
		$signed = Zend_Registry::get('Facebook_Signed');
		$formState = array();
		
		$formContact = new Hm_Form_ContactarHousemarket($signed,$formState);
		$formContact->run();
		$this->view->formContact = $formContact;
		
		$this->view->urlInstall = $this->getInstallUrl();
	}
	
	public function terminosycondicionesAction(){
		$this->appendStylesheet('/css/landing.css');
		$this->view->urlInstall = $this->getInstallUrl();
	}
}

?>