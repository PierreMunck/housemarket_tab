<?php

class MigrateController extends Hm_MainController {
	
	public function indexAction(){
		$this->appendStylesheet('/css/no_tab.css');
		
		
		$message = '<strong>&iexcl;Ya casi agregas Housemarket Tab!</strong><br/>';
		$message .= 'Estas a un paso de agregar un buscador de propiedades a tu p&aacute;gina de Facebook. Para completar la instalaci&oacute;n haz clic en instalar.';
		$this->view->message = $message;
		$this->view->pageid = $_GET['page_id'];
	}
	
	public function executeAction(){
		
		$this->appendStylesheet('/css/no_tab.css');
		//$pageId = '123122201143956';
		$pageId = $_GET['page_id'];
		$old_appId = '120449844656848';
		if( $this->HasAccess($this->Facebook_Signed->get('user_id'),$pageId)){
			try{
				// creation du token del aplicacion
				$this->app->setPageAccessToken($this->Facebook_Signed->get('user_id'),$pageId);
				
				// get informacion de pagina
				$pageBrokerInfo = $this->app->api('/'. $pageId);
				
				// delete old button
				if($this->existButtonInPage($pageId,$old_appId)){
					$graph = '/'. $pageId .'/tabs/app_'. $old_appId;
					$this->app->api($graph,'DELETE');
				}
				
				// push buton on  tab
				if(!$this->existButtonInPage($pageId,$this->cfg['facebook_appid'])){
					$graph = '/'. $pageId .'/tabs';
					$params = array(
						'app_id' => $this->cfg['facebook_appid']);
						//'custom_image_url' => $this->cfg['local_app_url'] .'img/logo_hm_tab.gif');
					$this->app->api($graph,'POST',$params);
				}
				
				// post message on wall
				$graph = '/'. $pageId .'/feed';
				$urlpage = $pageBrokerInfo['link'] .'?sk=app_'. $this->cfg['facebook_appid'];
		
				$params['link'] = $urlpage;
				//$params['message'] = 'encuentre nuestras propiedades';
		
				$this->app->api($graph, 'POST', $params);
		
				$this->app->setUserAccessToken();
				$message = '<strong>&iexcl;Gracias por agregar Housemarket Tab!</strong><br/>';
				$this->view->message = $message;
				$this->view->pageid = $_GET['page_id'];
			}catch(Exception $e){
				print_r($e);
			}
		}else{
			$pageBrokerInfo = $this->app->api('/'. $pageId);
			$message = '<strong>&#33;No eres administrador de esa pagina!</strong><br/>';
			$this->view->message = $message;
			$this->view->pageid = $_GET['page_id'];
		}
		
	}
	
	private function runFakeMail(){
		$listBroker = new Hm_Cli_PageServiceStatusList();
		$listBroker->loadAll(array('pageId'),true);
		$ClientePageInfo = new Hm_Cli_List();
		$ClientePageInfo->getClientePageinfo($listBroker->List);
		$ListFbEmail = array();
		$ListEmail = array();
		foreach($ClientePageInfo->List as $PageInfo){
			// special restriccion for this mode
			if( $PageInfo['uid'] !=  '100000041940062' && $PageInfo['uid'] !=  '512674512'){
				continue;
			}
			// mail para el usuario
			if(isset($PageInfo['Email'])){
				$ListEmail[$PageInfo['Email']] = array(
						'contact' => array(
								'name' => $PageInfo['NombreCliente'],
								'email' => $PageInfo['Email'],
						),
						'page' => array(
								'link' => 'https://tab.hmapp.com/migrate?page_id='. $PageInfo['pageid'],
								'name' => $PageInfo['pageName'],
						),
				);
			}
			// mail para la pagina
			if(isset($PageInfo['email'])){
				$ListEmail[$PageInfo['email']] = array(
						'contact' => array(
								'name' => $PageInfo['NombreCliente'],
								'email' => $PageInfo['email'],
						),
						'page' => array(
								'link' => 'https://tab.hmapp.com/migrate?page_id='. $PageInfo['pageid'],
								'name' => $PageInfo['pageName'],
						),
				);
			}
		
			//mail atraves de facebook
			if(isset($PageInfo['uid'])){
				$graph = '/'. $PageInfo['uid'];
				$UserInfo = $this->app->api($graph, 'GET');
				if(isset($UserInfo['username'])){
					$mail = $UserInfo['username'] .'@facebook.com';
					$ListFbEmail[$mail .'-'. $PageInfo['pageName']] = array(
							'contact' => array(
									'name' => $UserInfo['name'],
									'email' => $mail,
									'uid' => $PageInfo['uid'],
							),
							'page' => array(
									'link' => 'https://tab.hmapp.com/migrate?page_id='. $PageInfo['pageid'],
									'name' => $PageInfo['pageName'],
							),
					);
				}
			}
		
		}
		foreach($ListEmail as $key => $val){
			if($key != 'eduardosomarriba@hotmail.com'
					&& $key != 'pierre.munck@gmail.com'
					&& $key != 'eduardosomarriba@gmail.com'){
				unset($ListEmail[$key]);
			}
		}
		foreach($ListFbEmail as $key => $val){
			/*if($key != 'eduardosomarriba@facebook.com'
					&& $key != 'pierre.munck@facebook.com'){
				unset($ListFbEmail[$key]);
			}*/
		}
		foreach($ListEmail as $Email){
			$view = new Zend_View();
			$view->setScriptPath($this->cfg['resources']['view']['scriptPath']['default']);
			$mail = new Zend_Mail();
			$body = $view->partial('mail/migration.phtml',$Email);
			$mail->setBodyHtml($body);
			$body = $view->partial('mail/migration-text.phtml',$Email);
			$mail->setBodyText($body);
			$mail->setFrom('contact@hmapp.com', 'Housemarket App');
			$mail->addTo($Email['contact']['email'], $Email['contact']['name']);
			$mail->setSubject('New Housemarket Tab (optimizado para Timeline)');
			$mail->send();
			print_r('mail send to: '. $Email['contact']['email'] .' for page '. $Email['page']['name'] .'<br/>');
		}
		
		foreach($ListFbEmail as $Email){
			
			$mail = new Zend_Mail();
			$body = $view->partial('mail/migration.phtml',$Email);
			$mail->setBodyHtml($body);
			$body = $view->partial('mail/migration-text.phtml',$Email);
			$mail->setBodyText($body);
			$mail->setFrom('contact@hmapp.com', 'Housemarket App');
			$mail->addTo($Email['contact']['email'], $Email['contact']['name']);
			$mail->setSubject('New Housemarket Tab (optimizado para Timeline)');
			$mail->send();
			print_r('mail send to:'. $Email['contact']['email'] .'for page'. $Email['page']['name'] .'<br/>');
		}
	}
	
	private function runMail(){
		$listBroker = new Hm_Cli_PageServiceStatusList();
		$listBroker->loadAll(array('pageId'),true);
		$ClientePageInfo = new Hm_Cli_List();
		$ClientePageInfo->getClientePageinfo($listBroker->List);
		$ListFbEmail = array();
		$ListEmail = array();
		foreach($ClientePageInfo->List as $PageInfo){
			// mail para el usuario
			if(isset($PageInfo['Email'])){
				$ListEmail[$PageInfo['Email']] = array(
						'contact' => array(
								'name' => $PageInfo['NombreCliente'],
								'email' => $PageInfo['Email'],
						),
						'page' => array(
								'link' => 'https://tab.hmapp.com/migrate?page_id='. $PageInfo['pageid'],
								'name' => $PageInfo['pageName'],
						),
				);
			}
			// mail para la pagina
			if(isset($PageInfo['email'])){
				$ListEmail[$PageInfo['email']] = array(
						'contact' => array(
								'name' => $PageInfo['NombreCliente'],
								'email' => $PageInfo['email'],
						),
						'page' => array(
								'link' => 'https://tab.hmapp.com/migrate?page_id='. $PageInfo['pageid'],
								'name' => $PageInfo['pageName'],
						),
				);
			}
			//mail a traves de facebook
			if(isset($PageInfo['uid'])){
				$graph = '/'. $PageInfo['uid'];
				$UserInfo = $this->app->api($graph, 'GET');
				if(isset($UserInfo['username'])){
					$mail = $UserInfo['username'] .'@facebook.com';
					$ListFbEmail[$mail .'-'. $PageInfo['pageName']] = array(
							'contact' => array(
									'name' => $UserInfo['name'],
									'email' => $mail,
									'uid' => $PageInfo['uid'],
							),
							'page' => array(
									'link' => 'https://tab.hmapp.com/migrate?page_id='. $PageInfo['pageid'],
									'name' => $PageInfo['pageName'],
							),
					);
				}
			}
		}
		
		foreach($ListEmail as $Email){
			$view = new Zend_View();
			$view->setScriptPath($this->cfg['resources']['view']['scriptPath']['default']);
			$mail = new Zend_Mail();
			$body = $view->partial('mail/migration.phtml',$Email);
			$mail->setBodyHtml($body);
			$body = $view->partial('mail/migration-text.phtml',$Email);
			$mail->setBodyText($body);
			$mail->setFrom('info@hmapp.com', 'Housemarket App');
			$mail->addTo($Email['contact']['email'], $Email['contact']['name']);
			$mail->setSubject('New Housemarket Tab (optimizado para Timeline)');
			$mail->send();
			print_r('mail send to: '. $Email['contact']['email'] .' for page: '. $Email['page']['name'].' <br/>');
		}
		
		foreach($ListFbEmail as $Email){
			// aoorequest
			/*$graph = '/'. $Email['contact']['uid'] .'/apprequests';
			$post=array();
			$view = new Zend_View();
			$view->setScriptPath($this->cfg['resources']['view']['scriptPath']['default']);
			$body = $view->partial('mail/migration-fb-text.phtml',$Email);
			$post['message']=$body;
			$this->app->api($graph, 'POST',$post);*/
		
			// mail @facebook.com
			$view = new Zend_View();
			$view->setScriptPath($this->cfg['resources']['view']['scriptPath']['default']);
			$mail = new Zend_Mail();
			$body = $view->partial('mail/migration.phtml',$Email);
			$mail->setBodyHtml($body);
			$body = $view->partial('mail/migration-text.phtml',$Email);
			$mail->setBodyText($body);
			$mail->setFrom('info@hmapp.com', 'Housemarket App');
			$mail->addTo($Email['contact']['email'], $Email['contact']['name']);
			$mail->setSubject('New Housemarket Tab (optimizado para Timeline)');
			$mail->send();
			print_r('mail send to: '. $Email['contact']['email'] .' for page: '. $Email['page']['name'].' <br/>');
		}
	}
	
	public function runAction(){
		$uid = $this->Facebook_Signed->get('user_id');
		if(isset($this->uri[3])){
			if( $uid !=  100000041940062 && $uid !=  512674512){
				die();
			}
			if($this->uri[3] == 'fakemail'){
				$this->runFakeMail();
			}
			if($this->uri[3] == 'mail'){
				$this->runMail();
			}
			if($this->uri[3] == 'init'){
				$this->runInit();
			}
			if($this->uri[3] == 'listpage'){
				$this->runListPage();
			}
			if($this->uri[3] == 'chargemail'){
				$this->runChargeMail();
			}
		}
	}
	
	private function runChargeMail(){
		$listBroker = new Hm_Cli_PageServiceStatusList();
		$listBroker->loadAll(array('pageId'),true);
		$ClientePageInfo = new Hm_Cli_List();
		$ClientePageInfo->getClientePageinfo($listBroker->List);
		$ListFbEmail = array();
		$ListEmail = array();
		foreach($ClientePageInfo->List as $PageInfo){
			//mail a traves de facebook
			if(isset($PageInfo['uid'])){
				$graph = '/'. $PageInfo['uid'];
				$UserInfo = $this->app->api($graph, 'GET');
				if(isset($UserInfo['username'])){
					$mail = $UserInfo['username'] .'@facebook.com';
					$ListFbEmail[$mail .'-'. $PageInfo['pageName']] = array(
							'contact' => array(
									'name' => $UserInfo['name'],
									'email' => $mail,
									'uid' => $PageInfo['uid'],
							),
							'page' => array(
									'link' => 'https://tab.hmapp.com/migrate?page_id='. $PageInfo['pageid'],
									'name' => $PageInfo['pageName'],
							),
					);
				}
			}
		}
		foreach($ListFbEmail as $Email){
			$view = new Zend_View();
			$view->setScriptPath($this->cfg['resources']['view']['scriptPath']['default']);
			$bodyHtml = $view->partial('mail/migration.phtml',$Email);
			$bodyText = $view->partial('mail/migration-text.phtml',$Email);
			unset($view);
			$File = '../housemarkettab/application/data/mail/'. $Email['contact']['email'] .'.mailing';
			$fh = fopen($File, 'w') or die("can't open file");
			
			$info = array();
			$info['html'] = $bodyHtml;
			$info['text'] = $bodyText;
			$info['name'] = $Email['contact']['name'];
			$info['mail'] = $Email['contact']['email'];
			
			fwrite($fh, '<?php'."\n");
			fwrite($fh, '$mail = ');
			fwrite($fh, var_export ($info,true));
			fwrite($fh, ';');
			fwrite($fh, "\n".'?>');
			fclose($fh);
		}
	}
	
	private function runListPage(){
		// los usuario que pasa por la applicacion sera estociado en una tabla.
	}
	
	private function runInit(){
		$pageId = $_GET['page_id'];
		$old_appId = '120449844656848';
		try{
			// creation du token del aplicacion
			$this->app->setPageAccessToken($this->Facebook_Signed->get('user_id'),$pageId);
		
			$graph = '/'. $pageId .'/tabs/app_'. $this->cfg['facebook_appid'];
			$this->app->api($graph,'DELETE');
		
			// push buton on  tab
			$graph = '/'. $pageId .'/tabs';
			$params = array(
					'app_id' => $old_appId,
					'position' => 0,);
			//'custom_image_url' => $this->cfg['local_app_url'] .'img/logo_hm_tab.gif');
			$this->app->api($graph,'POST',$params);
		
			$this->app->setUserAccessToken();
		}catch(Exception $e){
			print_r($e);
		}
	}
	
	private function existButtonInPage($pageId,$appID){
		$tabBrokerInfo = $this->app->api('/'. $pageId .'/tabs');
		foreach ($tabBrokerInfo['data'] as $tab){
			$identifier = $pageId. '/tabs/app_' .$appID;
			if($tab['id'] == $identifier){
				return true;
			}
		}
		return false;
	}
	
	private function HasAccess($uid,$pageId){
		$url = '/'. $uid .'/accounts';
		$page_accounts = $this->app->api($url);
		foreach($page_accounts['data'] as $page){
			if($page['id'] == $pageId && isset($page['access_token'])){
				return true;
			}
		}
		return false;
	}
	
	
	public function scriptAction(){
		$uid = $this->Facebook_Signed->get('user_id');
		if(isset($this->uri[3])){
			if( $uid !=  100000041940062 && $uid !=  512674512){
				die();
			}
			if($this->uri[3] == 'limpiarpage'){
				$this->limpiarpage();
			}
		}
	}
	
	private function limpiarpage(){
		$routine = new Hm_Routine();
		$routine->pageServiceWithoutPage();
		
		foreach($routine->List as $page_service_status){
			$page_id = $page_service_status['pageId'];
			
			$pagelist =  new Hm_Fb_PageList();
			$pagelist->search(array('pageid' => $page_id));
			$uid = null;
			foreach($pagelist as $fbpage){
				$uid = $fbpage->uid;
			}
			
			$broker = new Hm_Cli_Cliente();
			$broker->search(array('Uid' => $uid));
			
			//page
			$data = array();
			$data['pageid'] = $page_service_status['pageId'];
			$data['nombre'] = $page_service_status['pageName'];
			$data['email'] = $broker->EMail;
			$data['youvideo'] = $broker->youVideo;
			$data['twitter'] = $broker->twitter;
			$data['telefono'] = $broker->TelefonoCliente;
			$data['webpage'] = $broker->WebPage;
			$data['telefonoempresa'] = $broker->TelefonoEmpresa;
			$data['nombreempresa'] = $broker->NombreEmpresa;
			
			print_r($data);
			//$page = new Hm_Cli_Page($data);
			//$page->save();
		}
	}
	
}

?>