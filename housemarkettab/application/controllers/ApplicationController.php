<?php

/**
 * Controller por defecto
 */
class ApplicationController extends Hm_MainController {
	
	
	private $mode = 'sale';
	private $adminMode = false;
	private $page_id = null;
	
	
	public function indexAction(){
		$page = $this->Facebook_Signed->get('page');
		if(isset($page)){
			$this->mapAction($page['id']);
		}
	}
	
	public function propiedadAction() {
		$this->view->url = 'http://propiedad.hmapp.com/show/propiedad/'. $_GET['propiedad_id'];
	}
	
	public function mapAction($page_id = null){
		
		$this->ActionBasePage($page_id);
		
		if($this->ControlActivacionPage($this->page_id)){
		
			$this->view->viewAdminMenu = $this->adminMode;
			
			if($this->mode == "rent"){
				$this->view->classRent = 'menu-for-rent-actif';
				$this->view->classSale = 'menu-for-sale';
			}else{
				$this->view->classRent = 'menu-for-rent';
				$this->view->classSale = 'menu-for-sale-actif';
			}
			
			$this->appendStylesheet('/css/tab_map_view.css');
			$this->loadJQueryPlugin('gmap');
			$this->view->headScript()->appendFile('/application/gmap/propiedadbypage/'.$this->page_id .'/'. $this->mode);
			$this->appendScriptFile('/js/gmap-ubicacion.js');
			$this->appendScriptFile('/js/hm-filter.js');
			$this->view->mode = $this->mode;
			$this->render('map');
		} else {
			$this->noPage();
			$this->render('notab');
		}
	}
	
	public function listpageAction($page_id = null){
		$this->ActionBasePage($page_id);
		if($this->ControlActivacionPage($this->page_id)){
			$this->view->viewAdminMenu = $this->adminMode;
	
			if($this->mode == "rent"){
				$this->view->classRent = 'menu-for-rent-actif';
				$this->view->classSale = 'menu-for-sale';
			}else{
				$this->view->classRent = 'menu-for-rent';
				$this->view->classSale = 'menu-for-sale-actif';
			}
			
			$this->appendStylesheet('/css/tab_list_view.css');
			$this->appendScriptFile('/js/jquery/jquery-1.3.2.js');
			$this->appendScriptFile('/js/hm-list-filter.js');
			
			$uids = $this->getUidsBrokerPage();
			$listpro = array();
			if(isset($uids) && !empty($uids)){
				$listpro = $this->getPropiedadByUids($uids);
			}
			$this->pager($listpro);
			$this->view->propiedadlist = $listpro;
			$this->view->mode = $this->mode;
		}else{
			$this->noPage();
			$this->render('notab');
		}
		
	}
	
	public function noPage(){
		$this->appendStylesheet('/css/no_tab.css');
		if($this->mode == 'noPageId'){
			$this->view->message = 'The page not exist';
		}
		if($this->adminMode == true){
			if($this->mode == 'RealizarPago'){
				$this->view->urlInstall = $this->getInstallUrl($this->cfg['local_app_url'] .'register?page_id='. $this->page_id .'&mode=pagar');
				$message = '<strong>Housemarket Tab no est&aacute; habilitado.</strong><br/>';
				$message .= 'Para activar Housemarket Tab en tu p&aacute;gina debes realizar el pago de la subscripci&oacute;n mensual. Utiliza nuestro medio de pago seguro para realizar la transferencia (soportado por PayPal).';
				$this->view->message = $message;
			}
			if($this->mode == 'AgregarPropiedad'){
				$message = '<strong>&iexcl;Housemarket Tab est&aacute; instalado exitosamente!</strong><br/>';
				$message .= 'Empieza a agregar tus propiedades para que tus fan puedan verlas. Si deseas hacer una carga masiva de propiedades cont&aacute;ctanos a info@hmapp.com.';
				$this->view->message = $message;
			}
		} else{
			$this->mode = 'Temporal';
			$message = '<strong>Vuelve pronto...</strong><br/>';
			$message .= 'Pronto encontraras en esta p&aacute;gina un buscador de propiedades con mapas de Google y otras herramientas de filtro para ayudarte encontrar la propiedad que andas buscando.';
			$this->view->message = $message;
		}
		$this->view->mode = $this->mode;
		
	}
	
	private function ControlActivacionPage($page_id = null){
		if(!isset($page_id)){
			$this->mode = 'noPageId';
			return false;
		}
		$pageServicestatus = new Hm_Cli_PageServiceStatus();
		$pageServicestatus->search(array('pageId' => $page_id));
		if(is_null($pageServicestatus->serviceExpirationDate)){
			$this->mode = 'RealizarPago';
			return false;
		}
		if (($expiration = strtotime($pageServicestatus->serviceExpirationDate)) === false) {
			$this->mode = 'noPageId';
			return false;
		}
		if($expiration < time()){
			$this->mode = 'RealizarPago';
			return false;
		}
		
		$checker = new Hm_Checker();
		if($checker->pageHasPropiedad($page_id,$this->mode)){
			return true;
		}else{
			$this->mode = 'AgregarPropiedad';
			return false;
		}
		
		return false;
	}
	
	private function ActionBasePage($page_id = null){
		$signed = $this->Facebook_Signed;
		$page = $signed->get('page');
		$this->page_id = $page_id;
		if(isset($page['id'])  && !isset($page_id)){
			$this->page_id = $page['id'];
		}
		$this->view->getPageId = '';
		if(isset($_GET['page_id'])){
			$this->page_id = $_GET['page_id'];
			$this->view->getPageId = 'page_id='. $this->page_id;
		}
		$this->view->pageId = $this->page_id;
		$this->adminMode = false;
		$uid = $signed->get('user_id');
		if(isset($uid) && isset($this->page_id) && $this->isAdmin($uid, $this->page_id)){
			
			$this->adminMode = true;
		}
		
		if(isset($_GET['mode'])){
			$this->mode = $_GET['mode'];
		}
		if($this->page_id == '112757475509748'){
			$redirect = '<script type="text/javascript">';
			$redirect .= 'window.location = "http://tab.hmapp.com/housemarket/map"';
			$redirect .= '</script>';
			print($redirect);
			die();
		} 
	}
	
	
	private function getUidsBrokerPage($page_id = null){
		if(!isset($page_id) && isset($this->page_id)){
			$page_id = $this->page_id;
		}
		$uids = array();
		$pagelist =  new Hm_Fb_PageList();
		$pagelist->search(array('pageid' => $page_id));
		$uid = null;
		foreach($pagelist as $fbpage){
			$uids[] = $fbpage->uid;
		}
		return $uids;
	}
	
	private function getPropiedadByUids($uids){
		$listpro = array();
		$propiedadList = new Hm_Pro_PropiedadList();
		// en Venta action = S (use mode)
		$action = array('S','B');
		if($this->mode == 'rent'){
			$action = array('R','B');
		}
		$propiedadList->addfiltro('Accion', $action);
		$propiedadList->addfiltro('Latitud', 'NOT NULL');
		$propiedadList->addfiltro('Longitud', 'NOT NULL');
		$propiedadList->addfiltro('FechaInactiva', 'NULL');
		if(isset($_GET['Type'])){
			$propiedadList->addfiltro('NombreEN',$_GET['Type'],'=','catcategoria');
		}
		$campo = 'PrecioVenta';
		if($this->mode == 'rent'){
			$campo = 'PrecioAlquiler';
		}
		if(isset($_GET['PrecioMin'])){
			$propiedadList->addfiltro($campo,$_GET['PrecioMin'],'>=');
		}
		if(isset($_GET['PrecioMax'])){
			$propiedadList->addfiltro($campo,$_GET['PrecioMax'],'<=');
		}
		if(isset($_GET['AreaMin'])){
			$propiedadList->addfiltro('Area',$_GET['AreaMin'],'>=');
		}
		if(isset($_GET['AreaMax'])){
			$propiedadList->addfiltro('Area',$_GET['AreaMax'],'<=');
		}
		if(isset($_GET['Cuarto'])){
			$propiedadList->addfiltro('cuarto',$_GET['Cuarto'],'>=','atributopropiedad');
		}
		if(isset($_GET['Bano'])){
			$propiedadList->addfiltro('bano',$_GET['Curato'],'>=','atributopropiedad');
		}
		
		$propiedadList->addCampoThumbnail();
		$propiedadList->search(array('Uid' => $uids),true,0);
		$propiedadList->lightload();
		
		// agregar el precio segun el modo
		foreach($propiedadList as $key => $propiedad){
			$propiedad->detallePrecio = $propiedad->detallePrecioVentaSafe;
			if($this->mode == 'rent'){
				$propiedad->detallePrecio = $propiedad->detallePrecioAlquilerSafe;
			}
			$propiedadList->List[$key] = $propiedad;
		}
		//prin_r($propiedadList->List);
		return $propiedadList->List;
	}
	
	public function gmapAction($page_id = null){
		$this->view->script = '';
		
		if($this->uri[3] == 'propiedadbypage'){
			$this->page_id = $this->uri[4];
			if(isset($this->uri[5])){
				$this->mode = $this->uri[5];
			}
			$zoom = null;
			if(isset($_GET['zoom'])){
				$zoom = $_GET['zoom'];
			}
			$latitude = null;
			if(isset($_GET['latitude'])){
				$latitude = $_GET['latitude'];
			}
			$longitude = null;
			if(isset($_GET['longitude'])){
				$longitude = $_GET['longitude'];
			}
			
			if(! $this->ControlActivacionPage($this->page_id)){
				print('no data');
				die();
			}
			
			$uids = $this->getUidsBrokerPage();
			
			if(isset($uids)){
				$propiedadList = new Hm_Pro_PropiedadList();
				// en Venta action = S (use mode)
				$action = array('S','B');
				if($this->mode == 'rent'){
					$action = array('R','B');
				}
				$propiedadList->addfiltro('Accion', $action);
				$propiedadList->addfiltro('Latitud', 'NOT NULL');
				$propiedadList->addfiltro('Longitud', 'NOT NULL');
				$propiedadList->addfiltro('FechaInactiva', 'NULL');
				
				//filtro de coordinates
				$coordinates = null;
				if(isset($latitude) && isset($longitude) && isset($zoom)){
					// calcul de los coordinates
				}
				if ($coordinates != null && is_array($coordinates)) {
					$propiedadList->loationFiltro($coordinates);
				}
				
				$propiedadList->addCampoThumbnail();
				$propiedadList->search(array('Uid' => $uids),true,0);
				
				// cluster
				$propiedadList->lightload();
				
				$this->view->script = "$().ready(function(){\n" .
					"\t$(\"#mapsearch\").gMap({\n";
				if(!empty($propiedadList) && $propiedadList->size() > 0){
					$this->view->script .= "\t\tmarkers: [\n";
					foreach($propiedadList as $propiedad){
						$this->view->script .="{\n".
							"latitude: ". $propiedad->Latitud .",".
							"longitude: ". $propiedad->Longitud .",".
							"html: \"". $this->burbujaGmap($propiedad) ."\",".
							"icon: {".
								"image: \"/img/gmap_pin.png\",".
								"iconsize: [29, 29],".
								"iconanchor: [12,29],".
								"infowindowanchor: [12, 0] }\n".
							"},\n";
					}
					if(!empty($propiedadList->cluster)){
						foreach($propiedadList->$cluster as $cluster){
							$this->view->script .="{\n".
									"latitude: ". $cluster->Latitud .",".
									"longitude: ". $cluster->Longitud .",".
									"icon: {".
									"image: \"/img/gmap_pinazul.png\",".
									"iconsize: [29, 29],".
									"iconanchor: [12,29],".
									"infowindowanchor: [12, 0] }\n".
									"},\n";
						}
					}
					$this->view->script .= "\t],\n";
				}
				$this->view->script .= "\t\tzoom: 10\n".
					"\t});".
					"});";
			}
		}
		print($this->view->script);
		die();
	}
	
	public function filterAction($page_id = null){
		$this->view->script = '';
	
		if($this->uri[3] == 'propiedadbypage'){
			$this->page_id = $this->uri[4];
			if(isset($this->uri[5])){
				$this->mode = $this->uri[5];
			}
			$zoom = null;
			if(isset($_GET['zoom'])){
				$zoom = $_GET['zoom'];
			}
			$latitude = null;
			if(isset($_GET['latitude'])){
				$latitude = $_GET['latitude'];
			}
			$longitude = null;
			if(isset($_GET['longitude'])){
				$longitude = $_GET['longitude'];
			}
			if(! $this->ControlActivacionPage($this->page_id)){
				print('no data');
				die();
			}
			$uids = $this->getUidsBrokerPage();
	
			if(isset($uids)){
				$propiedadList = new Hm_Pro_PropiedadList();
				// en Venta action = S (use mode)
				$action = array('S','B');
				if($this->mode == 'rent'){
					$action = array('R','B');
				}
				$propiedadList->addfiltro('Accion', $action);
				$propiedadList->addfiltro('Latitud', 'NOT NULL');
				$propiedadList->addfiltro('Longitud', 'NOT NULL');
				$propiedadList->addfiltro('FechaInactiva', 'NULL');
				
				if(isset($_GET['Type'])){
					$propiedadList->addfiltro('NombreEN',$_GET['Type'],'=','catcategoria');
				}
				$campo = 'PrecioVenta';
				if($this->mode == 'rent'){
					$campo = 'PrecioAlquiler';
				}
				if(isset($_GET['PrecioMin'])){
					$propiedadList->addfiltro($campo,$_GET['PrecioMin'],'>=');
				}
				if(isset($_GET['PrecioMax'])){
					$propiedadList->addfiltro($campo,$_GET['PrecioMax'],'<=');
				}
				if(isset($_GET['AreaMin'])){
					$propiedadList->addfiltro('Area',$_GET['AreaMin'],'>=');
				}
				if(isset($_GET['AreaMax'])){
					$propiedadList->addfiltro('Area',$_GET['AreaMax'],'<=');
				}
				if(isset($_GET['Cuarto'])){
					$propiedadList->addfiltro('cuarto',$_GET['Cuarto'],'>=','atributopropiedad');
				}
				if(isset($_GET['Bano'])){
					$propiedadList->addfiltro('bano',$_GET['Curato'],'>=','atributopropiedad');
				}
				
				//filtro de coordinates
				$coordinates = null;
				if(isset($latitude) && isset($longitude) && isset($zoom)){
					// calcul de los coordinates
				}
				if ($coordinates != null && is_array($coordinates)) {
					$propiedadList->loationFiltro($coordinates);
				}
				
				$propiedadList->addCampoThumbnail();
				$propiedadList->search(array('Uid' => $uids),true,0);
				
				// cluster
				$propiedadList->lightload();
	
				$this->view->script = "{\n";
				if(!empty($propiedadList) && $propiedadList->size() > 0){
					$this->view->script .= "\t\tmarkers: [\n";
					foreach($propiedadList as $propiedad){
						$this->view->script .="{\n".
								"latitude: ". $propiedad->Latitud .",".
								"longitude: ". $propiedad->Longitud .",".
								"html: \"". $this->burbujaGmap($propiedad) ."\",".
								"icon: {".
								"image: \"/img/gmap_pin.png\",".
								"iconsize: [29, 29],".
								"iconanchor: [12,29],".
								"infowindowanchor: [12, 0] }\n".
								"},\n";
					}
					if(!empty($propiedadList->cluster)){
						foreach($propiedadList->cluster as $cluster){
							$this->view->script .="{\n".
									"latitude: ". $cluster->Latitud .",".
									"longitude: ". $cluster->Longitud .",".
									"icon: {".
									"image: \"/img/gmap_pinazul.png\",".
									"iconsize: [29, 29],".
									"iconanchor: [12,29],".
									"infowindowanchor: [12, 0] }\n".
									"},\n";
						}
					}
					$this->view->script .= "\t],\n";
				}
				$this->view->script .= "\t\tzoom: 10\n".
						"\t}";
			}
		}
		print($this->view->script);
		die();
	}
	
	private function burbujaGmap($propiedad){
		$params = array();
		$params['thumbPhoto'] = $propiedad->thumbPhoto;
		$params['NombreCategoria'] = $propiedad->NombreCategoria;
		$params['AtributoCuarto'] = $propiedad->AtributoCuarto;
		$params['AtributoBano'] = $propiedad->AtributoBano;
		$params['AtributoParqueo'] = $propiedad->AtributoParqueo;
		$params['LabelArea'] = $propiedad->LabelArea;
		$params['LabelAreaLote'] = $propiedad->LabelAreaLote;
		$params['CodigoPropiedad'] = $propiedad->CodigoPropiedad;
		
		$params['detallePrecio'] = $propiedad->detallePrecioVentaSafe;
		if($this->mode == 'rent'){
			$params['detallePrecio'] = $propiedad->detallePrecioAlquilerSafe;
		}
		$partial = $this->view->partial('application/burbujaGmap.phtml',$params);
		$slashed_string = addslashes($partial);
		return $slashed_string;
	}
	
	public function precioselectvalAction($page_id = null){
		$data = array();
		if(isset($this->uri[3])){
			if($this->uri[3] == 'rent'){
				$data = array(
						'500' => '500',
						'1000' => '1000',
						'1500' => '1500',
						'2000' => '2000',
						'2500' => '2500',
						'3000' => '3000',
						'3500' => '3500',
						'4000' => '4000',
						'4500' => '4500',
						'5000' => '5K',
						'6000' => '6k',
						'7000' => '7k',
						'8000' => '8k',
						'9000' => '9k',
						'10000' => '10k',
						'15000' => '15k',
						'20000' => '20k',
						'25000' => '25k',
						'30000' => '30k',
						'40000' => '40k',
						'50000' => '50k',
				);
			}
			if($this->uri[3] == 'sale'){
				$data = array(
						'50000' => '50k',
						'100000' => '100k',
						'150000' => '150k',
						'200000' => '200k',
						'250000' => '250k',
						'300000' => '300k',
						'350000' => '350k',
						'400000' => '400k',
						'450000' => '450k',
						'500000' => '500K',
						'750000' => '750k',
						'1000000' => '1M',
						'1500000' => '1.5M',
						'2000000' => '2M',
						'3000000' => '3M',
						'4000000' => '4M',
						'5000000' => '5M',
						'7500000' => '7.5M',
						'10000000' => '10M',
						'20000000' => '20M',
						'30000000' => '30M',
						'40000000' => '40M',
						'50000000' => '50M',
				);
			}
		}
		print(json_encode($data));
		die();
	}
	
	public function areaselectvalAction($page_id = null){
		$data = array();
		if(isset($this->uri[3])){
			if($this->uri[3] == 'ft2'){
				$data = array(
						'500' => '500 ft2',
						'1000' => '1,000 ft2',
						'1500' => '1,500 ft2',
						'2000' => '2,000 ft2',
						'2500' => '2,500 ft2',
						'3000' => '3,000 ft2',
						'3500' => '3,500 ft2',
						'4000' => '4,000 ft2',
						'4500' => '4,500 ft2',
						'5000' => '5,000 ft2',
						'10000' => '1 Acre',
						'50000' => '5 Acre',
						'500000' => '50 Acre',
						'100000' => '100 Acre',
				);
			}
			if($this->uri[3] == 'mt2'){
				$data = array(
						'50' => '50 mt2',
						'100' => '100 mt2',
						'150' => '150 mt2',
						'200' => '200 mt2',
						'250' => '250 mt2',
						'300' => '300 mt2',
						'350' => '350 mt2',
						'400' => '400 mt2',
						'450' => '450 mt2',
						'500' => '500 mt2',
						'1000' => '1,000 mt2',
						'5000' => '5,000 mt2',
						'10000' => '1 hect',
						'50000' => '5 hect',
						'500000' => '50 hect',
						'1000000' => '100 hect',
				);
			}
		}
		print(json_encode($data));
		die();
	}
	
}