<?php

/**
 * Controller por defecto
 */
class PaypalController extends Zend_Controller_Action {

    /**
     *
     * @var <type>
     */
    protected $_flashMessenger = null;
    /**
     *
     * @var
     */
    protected $_redirector = null;

    /**
     * Inicializacion del controller
     */
    public function init() {
    	parent::init();
        $this->_redirector = $this->_helper->getHelper('Redirector');
    }

    public function indexAction(){
    $p = new Paypal_Class();             // initiate an instance of the class
    
    //configuration conneccion paypal
    //********* Certification ***********//
    //test
    /*$p->paypal_url = 'https://www.sandbox.paypal.com/cgi-bin/webscr';
    $p->addcert(APPLICATION_PATH .'/data/cert', 'paypal_private.pem', 'paypal_public.pem', 'paypal_cert_test.pem');
    $p->add_field('business', 'test@hmapp.com');
    $p->add_field('cert_id', 'X4A4YT9Q68E4S');*/
    
    //prod subscriptions@hmapp.com
    /*$p->paypal_url = 'https://www.paypal.com/cgi-bin/webscr';
    $p->addcert(APPLICATION_PATH .'/data/cert', 'paypal_private.pem', 'paypal_public.pem', 'paypal_cert.pem');
    $p->add_field('business', 'subscriptions@hmapp.com');
    $p->add_field('cert_id', '6HSE8M3V57DSG');*/
    
    //prod tab@hmapp.com
    $p->paypal_url = 'https://www.paypal.com/cgi-bin/webscr';
    $p->addcert(APPLICATION_PATH .'/data/cert', 'paypal_private.pem', 'paypal_public.pem', 'paypal_cert.pem');
    $p->add_field('business', 'tab@hmapp.com');
    $p->add_field('cert_id', '5NQ4MPREQPBVQ');
    
	$this_script = 'https://tab.hmapp.com/paypal';
	
	$this->view->headLink()->appendStylesheet('/css/main.css');
	$this->view->headLink()->appendStylesheet('/css/paypal.css');
	
	// if there is not action variable, set the default action of 'process'
	if (empty($_GET['action'])) $_GET['action'] = 'process';
	
	switch ($_GET['action']) {
	    
	   case 'process':      // Process and order...
	   	  // TODO buy multi mes
	   	  $fb_user_id = $_GET['user_id'];
	   	  $page_id = $_GET['page_id'];
	   	  // custom value uid facebook para buscar la pagina
	   	  $p->add_field('custom', $fb_user_id. ',' .$page_id);
	   	  $p->add_field('return', $this_script.'?action=success');
	   	  $p->add_field('cancel_return', $this_script.'?action=cancel');
	   	  $p->add_field('notify_url', $this_script.'/ipn');
	   	  
	   	  //********* Configuracion Paypal ********//
	   	  $p->add_field('cmd', '_xclick-subscriptions');
	      
	      // Name
	      $p->add_field('item_name', 'Housemarket Tab');
	      $p->add_field('no_note', '1');
	      $p->add_field('no_shipping', '1');
	      $p->add_field('currency_code', 'USD');
	      $p->add_field('rm', '2');
	      // price
	      $p->add_field('a3', '19.99');
	      // periode 1M = 1 mes 
	      $p->add_field('p3', '1');
	      $p->add_field('t3', 'M');
	      
	      // auto recurent 
	      $p->add_field('src', '1');  // auto renovate
	      //$p->add_field('srt', '12'); // durante 1 a�os
	      $p->add_field('sra', '1'); // reintentar en caso de problema antes de cancelar
	      //**************************************//
	      
	      $this->view->fields = $p->fields;
	      $this->view->paypal_url = $p->paypal_url;
	      $this->view->body_onLoad = 'document.forms[\'paypal_form\'].submit();'; // submit the fields to paypal
	      $this->view->encrypted_buton = $p->get_encrypted();
	      $log = Zend_Registry::get('log');
	      $log->info($p->senddata);
	      $this->render('process');
	      //$p->submit_paypal_post();
	      //$p->dump_fields();      // for debugging, output a table of all the fields
	      break;
	      
	   case 'success':      // Order was successful...
	   
	      // This is where you would probably want to thank the user for their order
	      // or what have you.  The order information at this point is in POST 
	      // variables.  However, you don't want to "process" the order until you
	      // get validation from the IPN.  That's where you would have the code to
	      // email an admin, update the database with payment status, activate a
	      // membership, etc.
	   	  $this->render('success');
	   	  list($fb_user_id, $page_id) = explode (',',$_GET['cm']);
	   	  $this->view->pageid = $page_id;
	   	   
	      // You could also simply re-direct them to another page, or your own 
	      // order status page which presents the user with the status of their
	      // order based on a database (which can be modified with the IPN code 
	      // below).
	      
	      break;
	      
	   case 'cancel':       // Order was canceled...
	
	      // The order was canceled before being completed.
	   	  $this->render('cancel');
	      
	      break;
	      
	   case 'ipn':          // Paypal is calling page for IPN validation...
	      // 
	      $this->ipnAction($p);
	      break;
	 }
	}
	
	public function ipnAction($p = null){
		if(!isset($p)){
			$p = new Paypal_Class();             // initiate an instance of the class
			//$p->paypal_url = 'https://www.sandbox.paypal.com/cgi-bin/webscr';   // testing paypal url
			$p->paypal_url = 'https://www.paypal.com/cgi-bin/webscr';     // paypal url
		}
		
		if ($p->validate_ipn()) {
			// custom uid
			$custom = $p->ipn_data['custom'];
			list($fb_user_id, $page_id) = explode (',',$custom);
			
			//type de repuesta
		
			//TODO validate the page del user id
			if(!isset($page_id)){
				$p->log_ipn_var('['.date('m/d/Y g:i A').'] - '. $p->ipn_data['txn_type']. ' :: '.$custom .' INVALID!\n');
				die();
			}
			
			$p->log_ipn_var($p->ipn_data['txn_type']. ' => [ page => '.$page_id.' user => '.$fb_user_id.']');
			
			$pageServicestatus = new Hm_Cli_PageServiceStatus();
			$pageServicestatus->search(array('pageId' => $page_id));
			
			switch($p->ipn_data['txn_type']){
				case 'subscr_signup':
					// el usuario valido la suscripcion
					break;
				case 'subscr_payment':
					// el usuario pago su suscriptcion
					$pageServicestatus->serviceStatus = 1;
					if(!isset($pageServicestatus->serviceStartDate)){
						$pageServicestatus->serviceStartDate = time();
					}else{
						$start = strtotime($pageServicestatus->serviceStartDate);
						$pageServicestatus->serviceStartDate = $start;
					}
					$p->log_ipn_var($pageServicestatus->serviceExpirationDate);
					if(isset($pageServicestatus->serviceExpirationDate)){
						$expiration = strtotime($pageServicestatus->serviceExpirationDate);
						$pageServicestatus->serviceExpirationDate = $expiration + 2592000;
					}else{
						$pageServicestatus->serviceExpirationDate = time() + 2592000;
					}
					$pageServicestatus->save();
					
					// mail de pago
					$mail = new Zend_Mail();
					$body =  "esa subscriptions ha sido recibido\n";
					$body .= "de ".$p->ipn_data['payer_email']." el ".date('m/d/Y');
					$body .= " a la ".date('g:i A')."\n";
					$body .= " para la pagina ". $pageServicestatus->pageName .' ('. $page_id .')'."\n";
					$body .="\nDetailes:\n";
					foreach ($p->ipn_data as $key => $value) {
						$body .= "\n$key: $value";
					}
					$mail->setBodyText($body);
					$mail->setFrom('tab@hmapp.com', 'Housemarket tab');
					//$mail->addTo('pierre.munck@gmail.com', 'Pierre Munck');
					$mail->addTo('info@hmapp.com', 'Housemarket');
					$mail->addBcc('eduardo.somarriba@gmail.com', 'Eduardo Sommariba');
					$mail->addBcc('pierre.munck@gmail.com', 'Pierre Munck');
					$mail->setSubject('[Housemarket Tab IPN] pago recibido '. $p->ipn_data['payer_email']);
					$mail->send();
					
					break;
				case 'subscr_cancel':
				case 'subscr_eot':
					// el usuario cancelo su suscripcion o la suscripcion llego a su fin
					$pageServicestatus->serviceStatus = 0;
					if(!isset($pageServicestatus->serviceStartDate)){
						$pageServicestatus->serviceStartDate = time();
					}else{
						$start = strtotime($pageServicestatus->serviceStartDate);
						$pageServicestatus->serviceStartDate = $start;
					}
					$pageServicestatus->serviceExpirationDate = time();
					$pageServicestatus->save();
					
					// mail de cancelacion
					$mail = new Zend_Mail();
					$body =  "esa subscriptions ha sido cancelado\n";
					$body .= "de ".$p->ipn_data['payer_email']." el ".date('m/d/Y');
					$body .= " a la ".date('g:i A')."\n";
					$body .= " para la pagina ". $pageServicestatus->pageName .' ('. $page_id .')'."\n";
					$body .="\nDetailes:\n";
					foreach ($p->ipn_data as $key => $value) {
						$body .= "\n$key: $value";
					}
					$mail->setBodyText($body);
					$mail->setFrom('tab@hmapp.com', 'Housemarket tab');
					//$mail->addTo('pierre.munck@gmail.com', 'Pierre Munck');
					$mail->addTo('info@hmapp.com', 'Housemarket');
					$mail->addBcc('eduardo.somarriba@gmail.com', 'Eduardo Sommariba');
					$mail->addBcc('pierre.munck@gmail.com', 'Pierre Munck');
					$mail->setSubject('[Housemarket Tab IPN] cancelacion de subscripcion '. $p->ipn_data['payer_email']);
					$mail->send();
					break;
				case 'subscr_failed':
				case 'subscr_modify':
					// caso de error y el usuario modifico su cuenta
					break;
			}
		}
		die();
	}
}

?>