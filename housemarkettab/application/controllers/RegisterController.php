<?php

/**
 * Controller por defecto
 */
class RegisterController extends Hm_MainController {

    /**
     *
     * @var <type>
     */
    protected $_flashMessenger = null;
    /**
     *
     * @var
     */
    protected $_redirector = null;

    /**
     * Inicializacion del controller
     */
    public function init() {
    	parent::init();
        $this->_redirector = $this->_helper->getHelper('Redirector');
        //$this->_flashMessenger = $this->_helper->getHelper('FlashMessenger');
    }

    public function indexAction(){
    	$this->appendStylesheet('/css/register.css');
    	$this->Facebook_Signed->getUserInfo();
    	
    	// get page_id from url
    	if(!isset($_POST['PageId']) && isset($_GET['page_id'])){
    		$_POST['PageId'] = $_GET['page_id'];
    	}
    	$this->view->title = 'Facturaci&oacuten autom&aacutetica ';
    	$this->view->text = 'Hemos optado por un m&eacutetodo de facturaci&oacuten autom&aacutetica para mantener activo y funcional HM Tab en su p&aacutegina de Facebook. Usted puede cancelar su plan con nosotros en cualquier momento (hasta 24 horas antes de la fecha de facturaci&oacuten recurrente) sin ning&uacuten tipo de sanci&oacuten.';
    	if(isset($_GET['mode']) && $_GET['mode']= 'pagar'){
    		$this->view->title = 'Realizar pago';
    		$this->view->text = 'Esa action reactivara su Tab';
    	}
    	$this->view->formRegister = new Hm_Form_Register($this->Facebook_Signed, $_POST, $this->_request->getParams());
    }
    /**
     * Antes de procesar alguna accion
     */
    public function preDispatch() {
    	parent::preDispatch();
    }
    
    public function postAction() {
    	
    	//register de la pedida
    	$formRegister = new Hm_Form_Register($this->Facebook_Signed, $_POST,$this->_request->getParams());
    	$submit = false;
    	$codigoregister = 0;
    	if($formRegister->validate()){
    		$codigoregister = $formRegister->submit();
    		$submit = true;
    	}
    	else{
    		$this->appendStylesheet('/css/register.css');
    		$this->view->formRegister = $formRegister;
    		$this->render('index');
    	}
    	if($submit){
    		$page_id = $formRegister->getValue(PageId);
    		$user_id = $this->Facebook_Signed->get('user_id');
    		$this->_helper->redirector->gotoUrl('Paypal?user_id='.$user_id.'&page_id='. $page_id);
    	}
    }
    
    public function finalizarAction() {
    		
    }

}

?>