<?php

class HousemarketController extends Hm_MainController {
	
	private $mode = 'sale';
	private $adminMode = false;
	
	public function indexAction(){
		
	}
	
	public function mapAction(){

		$this->adminMode = false;
		$uid = $this->Facebook_Signed->get('user_id');
		if(isset($uid) && $uid == '12548'){
			$this->adminMode = true;
		}
		
		$this->mode = 'sale';
		if(isset($this->uri[3])){
			$this->mode = $this->uri[3];
		}
		
		$this->view->viewAdminMenu = $this->adminMode;
	
		if($this->mode == "rent"){
			$this->view->classRent = 'menu-for-rent-actif';
			$this->view->classSale = 'menu-for-sale';
		}else{
			$this->view->classRent = 'menu-for-rent';
			$this->view->classSale = 'menu-for-sale-actif';
		}
	
		$this->appendStylesheet('/css/tab_map_view.css');
		$this->loadJQueryPlugin('gmap');
		$this->view->headScript()->appendFile('/housemarket/gmap/'. $this->mode);
		$this->appendScriptFile('/js/gmap-ubicacion.js');
		$this->appendScriptFile('/js/hm-filter.js');
		$this->view->mode = $this->mode;
	}
	
	public function listpageAction($page_id = null){
		
		$this->adminMode = false;
		$uid = $this->Facebook_Signed->get('user_id');
		if(isset($uid) && $uid == '12548'){
			$this->adminMode = true;
		}
		
		$this->mode = 'sale';
		if(isset($this->uri[3])){
			$this->mode = $this->uri[3];
		}
		
		$this->view->viewAdminMenu = $this->adminMode;

		if($this->mode == "rent"){
			$this->view->classRent = 'menu-for-rent-actif';
			$this->view->classSale = 'menu-for-sale';
		}else{
			$this->view->classRent = 'menu-for-rent';
			$this->view->classSale = 'menu-for-sale-actif';
		}

		$this->appendStylesheet('/css/tab_list_view.css');
		$this->appendScriptFile('/js/jquery/jquery-1.3.2.js');
		$this->appendScriptFile('/js/hm-list-filter.js');

		$listpro = $this->getPropiedad();
		$this->pager($listpro);
		$this->view->propiedadlist = $listpro;
		$this->view->mode = $this->mode;
	}
	
	private function getPropiedad(){
		$listpro = array();
		$propiedadList = new Hm_Pro_PropiedadList();
		// en Venta action = S (use mode)
		$action = array('S','B');
		if($this->mode == 'rent'){
			$action = array('R','B');
		}
		
		$propiedadList->addfiltro('Latitud', 'NOT NULL');
		$propiedadList->addfiltro('Longitud', 'NOT NULL');
		$propiedadList->addfiltro('FechaInactiva', 'NULL');
		$propiedadList->addfiltro('FechaPublica', 'NOT NULL');
		$propiedadList->addorder('FechaPublica');
		
		if(isset($_GET['Type'])){
			$propiedadList->addfiltro('NombreEN',$_GET['Type'],'=','catcategoria');
		}
		$campo = 'PrecioVenta';
		if($this->mode == 'rent'){
			$campo = 'PrecioAlquiler';
		}
		if(isset($_GET['PrecioMin'])){
			$propiedadList->addfiltro($campo,$_GET['PrecioMin'],'>=');
		}
		if(isset($_GET['PrecioMax'])){
			$propiedadList->addfiltro($campo,$_GET['PrecioMax'],'<=');
		}
		if(isset($_GET['AreaMin'])){
			$propiedadList->addfiltro('Area',$_GET['AreaMin'],'>=');
		}
		if(isset($_GET['AreaMax'])){
			$propiedadList->addfiltro('Area',$_GET['AreaMax'],'<=');
		}
		if(isset($_GET['Cuarto'])){
			$propiedadList->addfiltro('cuarto',$_GET['Cuarto'],'>=','atributopropiedad');
		}
		if(isset($_GET['Bano'])){
			$propiedadList->addfiltro('bano',$_GET['Curato'],'>=','atributopropiedad');
		}
	
		$propiedadList->addCampoThumbnail();
		$propiedadList->search(array('Accion' => $action),true,3000);
		$propiedadList->lightload();
	
		// agregar el precio segun el modo
		foreach($propiedadList as $key => $propiedad){
			$propiedad->detallePrecio = $propiedad->detallePrecioVentaSafe;
			if($this->mode == 'rent'){
				$propiedad->detallePrecio = $propiedad->detallePrecioAlquilerSafe;
			}
			$propiedadList->List[$key] = $propiedad;
		}
		//prin_r($propiedadList->List);
		return $propiedadList->List;
	}
	
	public function gmapAction(){
		$this->view->script = '';
		
		if(isset($this->uri[3])){
			$this->mode = $this->uri[3];
		}
		$zoom = null;
		if(isset($_GET['zoom'])){
			$zoom = $_GET['zoom'];
		}
		$latitude = null;
		if(isset($_GET['latitude'])){
			$latitude = $_GET['latitude'];
		}
		$longitude = null;
		if(isset($_GET['longitude'])){
			$longitude = $_GET['longitude'];
		}

		$propiedadList = new Hm_Pro_PropiedadList();
		// en Venta action = S (use mode)
		$action = array('S','B');
		if($this->mode == 'rent'){
			$action = array('R','B');
		}
		$propiedadList->addfiltro('Latitud', 'NOT NULL');
		$propiedadList->addfiltro('Longitud', 'NOT NULL');
		$propiedadList->addfiltro('FechaInactiva', 'NULL');
		$propiedadList->addfiltro('FechaPublica', 'NOT NULL');
		$propiedadList->addorder('FechaPublica');
		
		//filtro de coordinates
		$coordinates = null;
		if(isset($latitude) && isset($longitude) && isset($zoom)){
			// calcul de los coordinates
		}
		if ($coordinates != null && is_array($coordinates)) {
			$propiedadList->loationFiltro($coordinates);
		}
		
		$propiedadList->addCampoThumbnail();
		$propiedadList->search(array('Accion' => $action),true,3000);
		
		$this->log->info(print_r($propiedadList->request,true));
		// cluster
		$propiedadList->lightload();
		
		$this->view->script = "$().ready(function(){\n" .
			"\t$(\"#mapsearch\").gMap({\n";
		if(!empty($propiedadList) && $propiedadList->size() > 0){
			$this->view->script .= "\t\tmarkers: [\n";
			foreach($propiedadList as $propiedad){
				$this->view->script .="{\n".
					"latitude: ". $propiedad->Latitud .",".
					"longitude: ". $propiedad->Longitud .",".
					"html: \"". $this->burbujaGmap($propiedad) ."\",".
					"icon: {".
						"image: \"/img/gmap_pin.png\",".
						"iconsize: [29, 29],".
						"iconanchor: [12,29],".
						"infowindowanchor: [12, 0] }\n".
					"},\n";
			}
			if(!empty($propiedadList->cluster)){
				foreach($propiedadList->$cluster as $cluster){
					$this->view->script .="{\n".
							"latitude: ". $cluster->Latitud .",".
							"longitude: ". $cluster->Longitud .",".
							"icon: {".
							"image: \"/img/gmap_pinazul.png\",".
							"iconsize: [29, 29],".
							"iconanchor: [12,29],".
							"infowindowanchor: [12, 0] }\n".
							"},\n";
				}
			}
			$this->view->script .= "\t],\n";
		}
		$this->view->script .= "\t\tzoom: 10\n".
			"\t});".
			"});";
		print($this->view->script);
		die();
	}
	
	public function filterAction($page_id = null){
		$this->view->script = '';
		
		if(isset($this->uri[3])){
			$this->mode = $this->uri[3];
		}
		
		$zoom = null;
		if(isset($_GET['zoom'])){
			$zoom = $_GET['zoom'];
		}
		$latitude = null;
		if(isset($_GET['latitude'])){
			$latitude = $_GET['latitude'];
		}
		$longitude = null;
		if(isset($_GET['longitude'])){
			$longitude = $_GET['longitude'];
		}

		$propiedadList = new Hm_Pro_PropiedadList();
		// en Venta action = S (use mode)
		$action = array('S','B');
		if($this->mode == 'rent'){
			$action = array('R','B');
		}
		$propiedadList->addfiltro('Latitud', 'NOT NULL');
		$propiedadList->addfiltro('Longitud', 'NOT NULL');
		$propiedadList->addfiltro('FechaInactiva', 'NULL');
		$propiedadList->addfiltro('FechaPublica', 'NOT NULL');
		$propiedadList->addorder('FechaPublica');

		if(isset($_GET['Type'])){
			$propiedadList->addfiltro('NombreEN',$_GET['Type'],'=','catcategoria');
		}
		$campo = 'PrecioVenta';
		if($this->mode == 'rent'){
			$campo = 'PrecioAlquiler';
		}
		if(isset($_GET['PrecioMin'])){
			$propiedadList->addfiltro($campo,$_GET['PrecioMin'],'>=');
		}
		if(isset($_GET['PrecioMax'])){
			$propiedadList->addfiltro($campo,$_GET['PrecioMax'],'<=');
		}
		if(isset($_GET['AreaMin'])){
			$propiedadList->addfiltro('Area',$_GET['AreaMin'],'>=');
		}
		if(isset($_GET['AreaMax'])){
			$propiedadList->addfiltro('Area',$_GET['AreaMax'],'<=');
		}
		if(isset($_GET['Cuarto'])){
			$propiedadList->addfiltro('cuarto',$_GET['Cuarto'],'>=','atributopropiedad');
		}
		if(isset($_GET['Bano'])){
			$propiedadList->addfiltro('bano',$_GET['Curato'],'>=','atributopropiedad');
		}

		//filtro de coordinates
		$coordinates = null;
		if(isset($latitude) && isset($longitude) && isset($zoom)){
			// calcul de los coordinates
		}
		if ($coordinates != null && is_array($coordinates)) {
			$propiedadList->loationFiltro($coordinates);
		}

		$propiedadList->addCampoThumbnail();
		$propiedadList->search(array('Accion' => $action),true,3000);

		// cluster
		$propiedadList->lightload();

		$this->view->script = "{\n";
		if(!empty($propiedadList) && $propiedadList->size() > 0){
			$this->view->script .= "\t\tmarkers: [\n";
			foreach($propiedadList as $propiedad){
				$this->view->script .="{\n".
						"latitude: ". $propiedad->Latitud .",".
						"longitude: ". $propiedad->Longitud .",".
						"html: \"". $this->burbujaGmap($propiedad) ."\",".
						"icon: {".
						"image: \"/img/gmap_pin.png\",".
						"iconsize: [29, 29],".
						"iconanchor: [12,29],".
						"infowindowanchor: [12, 0] }\n".
						"},\n";
			}
			if(!empty($propiedadList->cluster)){
				foreach($propiedadList->cluster as $cluster){
					$this->view->script .="{\n".
							"latitude: ". $cluster->Latitud .",".
							"longitude: ". $cluster->Longitud .",".
							"icon: {".
							"image: \"/img/gmap_pinazul.png\",".
							"iconsize: [29, 29],".
							"iconanchor: [12,29],".
							"infowindowanchor: [12, 0] }\n".
							"},\n";
				}
			}
			$this->view->script .= "\t],\n";
		}
		$this->view->script .= "\t\tzoom: 10\n".
				"\t}";
		print($this->view->script);
		die();
	}
	
	private function burbujaGmap($propiedad){
		$params = array();
		$params['thumbPhoto'] = $propiedad->thumbPhoto;
		$params['NombreCategoria'] = $propiedad->NombreCategoria;
		$params['AtributoCuarto'] = $propiedad->AtributoCuarto;
		$params['AtributoBano'] = $propiedad->AtributoBano;
		$params['AtributoParqueo'] = $propiedad->AtributoParqueo;
		$params['LabelArea'] = $propiedad->LabelArea;
		$params['LabelAreaLote'] = $propiedad->LabelAreaLote;
		$params['CodigoPropiedad'] = $propiedad->CodigoPropiedad;
	
		$params['detallePrecio'] = $propiedad->detallePrecioVentaSafe;
		if($this->mode == 'rent'){
			$params['detallePrecio'] = $propiedad->detallePrecioAlquilerSafe;
		}
		$partial = $this->view->partial('application/burbujaGmap.phtml',$params);
		$slashed_string = addslashes($partial);
		return $slashed_string;
	}
}

?>