<?php

/**
 * Error Controller
 */
class ErrorController extends Zend_Controller_Action {
    public function errorAction() {
    	try {
    		// minified and concatenated: errores.css
    		$this->view->headLink()->appendStylesheet('/css/no_tab.css');
    		$this->view->headLink()->appendStylesheet('/css/main.css');
    		$errors = $this->_getParam('error_handler');
    		// Default mensaje
    		$message = '<strong>Vuelve pronto...</strong><br/>';
    		$message .= 'Pronto encontraras en esta p&aacute;gina un buscador de propiedades con mapas de Google y otras herramientas de filtro para ayudarte encontrar la propiedad que andas buscando.';
    		$this->view->message = $message;
    		switch ($errors->type) {
    			case Zend_Controller_Plugin_ErrorHandler::EXCEPTION_NO_CONTROLLER:
    			case Zend_Controller_Plugin_ErrorHandler::EXCEPTION_NO_ACTION:
    				// 404 error -- controller or action not found
    				$this->view->message = $message;
    				if(Zend_Registry::isRegistered('log')){
    					$log = Zend_Registry::get('log');
    					$log->err(print_r($errors,true));
    				}
    				break;
    			default:
    				// application error
    				$this->view->message = $message;
	    			if(Zend_Registry::isRegistered('log')){
	    				$log = Zend_Registry::get('log');
	    				$log->err(print_r($errors,true));
	    			}
    			break;
    		}
    		
    		
    		
    	} catch(Exception $ex) {
    		if(Zend_Registry::isRegistered('log')){
    			$log = Zend_Registry::get('log');
    			$log->err(print_r($ex,true));
    		}
    	}
    }

}

?>