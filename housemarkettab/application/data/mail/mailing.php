<?php
echo 'start'."\n";

$APPLICATION_PATH = realpath(dirname(__FILE__) . '/../../..');

set_include_path(implode(PATH_SEPARATOR, array(
		realpath($APPLICATION_PATH . '/library/'),
)));

$dir = $APPLICATION_PATH .'/application/data/mail';
$i = 0;
date_default_timezone_set('America/Denver');
$LogFile = $APPLICATION_PATH .'/application/data/logs/mailing.log';
$log = fopen($LogFile, 'a');

include_once 'Zend/Mail.php';

foreach (glob($dir .'/*.mailing') as $filename) {
	if($i < 2){
		include_once $filename;
		// TODO uncoment send
		$Zmail = new Zend_Mail();
		$Zmail->setBodyHtml($mail['html']);
		$Zmail->setBodyText($mail['text']);
		$Zmail->setFrom('info@hmapp.com', 'Housemarket App');
		$Zmail->addTo($mail['mail'], $mail['name']);
		$Zmail->setSubject('New Housemarket Tab (optimizado para Timeline)');
		$Zmail->send();
		unlink($filename);
		$i++;
		fwrite($log, 'send mail to '. $mail['mail'] .' '. date("d/m/Y g:i a",time()) ."\n");
	}else{
		break;
	}
}
fclose($log);
exit(0);
?>