<?php 

class Hm_Parametros extends Hm_Db_Table {
	/**
	 * Nombre de la tabla
	 * @var String
	 */
	protected $_name = 'parametros';
	
	/**
	 * Nombre de la llave primaria de la tabla
	 * @var Integer
	 */
	protected $_primary = 'CodigoParametro';
	
	/**
	 * campo de la tabla
	 */
	public $CodigoParametro = null;
	public $NombreParametro = null;
	public $ValorParametro = null;
	
	/**
	 * descripcion de la Tabla
	 * @return multitype:string
	 */
	public function TableDescribe(){
		return array(
				'CodigoParametro' => 'int',
				'NombreParametro' => 'string',
				'ValorParametro' => 'string',
		);
	}

}
?>