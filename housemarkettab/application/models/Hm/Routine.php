<?php 

class Hm_Routine extends Zend_Db_Table {
	
	public $request = '';
	
	protected $_name = 'propropiedad';
	
	function __construct() {
		$this->_setAdapter('db');
	}
	/**
	 * descripcion de la Tabla
	* @return multitype:string
	*/
	public function pageServiceWithoutPage(){
		$select = $this->select();
		$select->setIntegrityCheck(false);
		$select->from('page_service_status');
		$select->where('pageId not in (select pageid from page)');
		$select->where('serviceStartDate is not null');
		
		$this->request = $select->assemble();
		
		$rs = $this->fetchAll($select);
		
		if(empty($rs)){
			return false;
		}
		
		$result = $rs->toArray();
		if(is_array($result) && isset($result[0])){
			$this->List = $result;
		}
		
		return false;
	}

}
?>