<?php 

class Hm_Cli_UserPref extends Hm_Db_Table {

	/**
	 * Nombre de la tabla
	 * @var String
	 */
	protected $_name = 'user_pref';

	/**
	 * Nombre de la llave primaria de la tabla
	 * @var Integer
	 */
	protected $_primary = 'uid';
	
	/**
	 * campo de la tabla
	 */
	public $uid = null;
	public $pais = null;

	/**
	 * descripcion de la Tabla
	* @return multitype:string
	*/
	public function TableDescribe(){
		return array(
				'uid' => 'int',
				'pais' => 'string',
		);
	}
	
}
?>