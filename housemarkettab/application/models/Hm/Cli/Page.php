<?php 

class Hm_Cli_Page extends Hm_Db_Table {

	/**
	 * Nombre de la tabla
	 * @var String
	 */
	protected $_name = 'page';

	/**
	 * Nombre de la llave primaria de la tabla
	 * @var Integer
	 */
	protected $_primary = 'id';
	
	/**
	 * campo de la tabla
	 */
 	public $id = null;
	public $pageid = null;
	public $nombre = null;
	public $email = null;
	public $youvideo = null;
	public $twitter = null;
	public $telefono = null;
	public $webpage = null;
	public $telefonoempresa = null;
	public $nombreempresa = null;

	/**
	 * descripcion de la Tabla
	* @return multitype:string
	*/
	public function TableDescribe(){
		return array(
				'id' => 'int',
				'pageid' => 'int',
				'nombre' => 'string',
				'email' => 'string',
				'youvideo' => 'string',
				'twitter' => 'string',
				'telefono' => 'string',
				'webpage' => 'string',
				'telefonoempresa' => 'string',
				'nombreempresa' => 'string',
		);
	}
	
	public function save() {
		// verificacion de la llave unica pageid
		if(isset($this->pageid)){
			if($exist = $this->search(array('pageid' => $this->pageid),true)){
				$this->id = $exist['id'];
			}
		}
		parent::save();
	}
}
?>