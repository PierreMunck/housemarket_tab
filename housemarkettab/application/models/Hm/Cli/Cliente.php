<?php

class Hm_Cli_Cliente extends Hm_Db_Table {
    /**
     * Nombre de la tabla
     * @var String
     */
    protected $_name = 'cliente';

    /**
     * Nombre de la llave primaria de la tabla
     * @var Integer
     */
    protected $_primary = 'CodigoCliente';

    /**
     * campo de la tabla
     */
    public $CodigoCliente = null;
    public $NombreCliente = null;
    public $TelefonoCliente = null;
    public $WebPage = null;
    public $EMail = null;
    public $NombreEmpresa = null;
    public $Uid = null;
    public $FechaRegistro = null;
    public $FechaCambio = null;
    public $TelefonoEmpresa = null;
    public $Origen = null;
    public $CargaMasiva = null;
    public $ComparteFavorito = null;
    public $FechaOtorgaCarga = null;
    public $FechaRevocaCarga = null;
    public $Broker = null;
    public $FechaSolicitaCarga = null;
    public $profilePageID = null;
    public $suscribe = null;
    public $youVideo = null;
    public $twitter = null;
    
    /**
     * campo facebook
     * @var unknown_type
     */
    
    public $FirstName = null;
    public $Name = null;
    public $CurrentLocation = null;
    public $PicSmall = null;
    public $PicBig = null;
    /**
     * descripcion de la Tabla
     * @return multitype:string
     */
    public function TableDescribe(){
    	return array(
    		'CodigoCliente' => 'int',
    		'NombreCliente' => 'string',
    		'TelefonoCliente' => 'string',
    		'WebPage' => 'string',
    		'EMail' => 'string',
    		'NombreEmpresa' => 'string',
    		'Uid' => 'int',
    		'FechaRegistro' => 'date',
    		'FechaCambio' => 'date',
    		'TelefonoEmpresa' => 'string',
    		'Origen' => 'string',
    		'CargaMasiva' => 'bool',
    		'ComparteFavorito' => 'bool',
    		'FechaOtorgaCarga' => 'date',
    		'FechaRevocaCarga' => 'date' ,
    		'Broker' => 'bool',
    		'FechaSolicitaCarga' => 'date',
    		'profilePageID' => 'int',
    		'suscribe' => 'bool',
    		'youVideo' => 'string',
    		'twitter' => 'string',
    	);
    }
    
    function __construct($cliId = null) {
    	if(is_int($cliId)){
    		$this->CodigoCliente = $cliId;
    	}
    	$cfg = Zend_Registry::get('config');
    	$this->PicBig = $cfg['local_app_url'] . "img/q_silhouette.gif";;
    	$this->PicSmall = $cfg['local_app_url'] . "img/q_silhouette.gif";
    	parent::__construct();
    }

    public function populate($values) {
    	parent::populate($values);
    	
    	// specific info from facebook
    	if(isset($this->Uid)){
	    	$facebookInfo = Hm_Facebook_User::getInstance();
	    	$facebookInfoValue = $facebookInfo->getDataUser($this->Uid, array('first_name', 'name', 'pic_big', 'current_location', 'pic_small'));
    		$this->FirstName = $facebookInfoValue['first_name'];
    		$this->Name = $facebookInfoValue['name'];
    		if (isset($facebookInfoValue['pic_small']) && strlen($facebookInfoValue['pic_small']) > 0) {
    			$this->PicSmall = $facebookInfoValue['pic_small'];
    		}
    		if (isset($facebookInfoValue['pic_big']) && strlen($facebookInfoValue['pic_big']) > 0) {
    			$this->PicBig = $facebookInfoValue['pic_big'];
    		}
    		if(isset($facebookInfoValue['current_location'])){
    			$this->CurrentLocation = $facebookInfoValue['current_location'];
    		}
    	}
    }
    
}

?>
