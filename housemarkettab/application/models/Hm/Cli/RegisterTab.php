<?php 

class Hm_Cli_RegisterTab extends Hm_Db_Table {

	/**
	 * Nombre de la tabla
	 * @var String
	 */
	protected $_name = 'CLIRegisterTab';

	/**
	 * Nombre de la llave primaria de la tabla
	 * @var Integer
	 */
	protected $_primary = 'CodigoRegister';
	
	/**
	 * campo de la tabla
	 */

	public $CodigoRegister = null;
	public $NombreCliente = null;
	public $Uid = null;
	public $EMail = null;
	public $TelefonoCliente = null;
	public $NombreEmpresa = null;
	public $TelefonoEmpresa = null;
	
	/**
	 * descripcion de la Tabla
	* @return multitype:string
	*/
	public function TableDescribe(){
		return array(
				'CodigoRegister' => 'int',
				'NombreCliente'=> 'string',
				'Uid' => 'int',
				'EMail' => 'string',
				'TelefonoCliente' => 'string',
				'NombreEmpresa' => 'string',
				'TelefonoEmpresa' => 'string',
		);
	}
	
}
?>