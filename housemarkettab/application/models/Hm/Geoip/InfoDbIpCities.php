<?php 

class Hm_Geoip_InfoDbIpCities extends Hm_Db_Table {

	/**
	 * Nombre de la tabla
	 * @var String
	 */
	protected $_name = 'geoipinfodb_ipcities';

	/**
	 * Nombre de la llave primaria de la tabla
	 * @var Integer
	 */
	protected $_primary = array('ipfrom','ipto');
	
	/**
	 * campo de la tabla
	 */
	public $ipfrom = null;
	public $ipto = null;
	public $cc = null;
	public $country = null;
	public $region = null;
	public $city = null;
	public $lat = null;
	public $lng = null;
	public $zip = null;

	/**
	 * descripcion de la Tabla
	* @return multitype:string
	*/
	public function TableDescribe(){
		return array(
				'ipfrom' => 'int',
				'ipto' => 'int',
				'cc' => 'string',
				'country' => 'string',
				'region' => 'string',
				'city' => 'string',
				'lat' => 'int',
				'lng' => 'int',
				'zip' => 'string',
		);
	}
	
}
?>