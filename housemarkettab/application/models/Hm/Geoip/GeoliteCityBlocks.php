<?php 

class Hm_GeoIp_GeoliteCityBlocks extends Hm_Db_Table {

	/**
	 * Nombre de la tabla
	 * @var String
	 */
	protected $_name = 'geolitecity_blocks';

	/**
	 * Nombre de la llave primaria de la tabla
	 * @var Integer
	 */
	protected $_primary = array('startIPNum','endIPNum');
	
	/**
	 * campo de la tabla
	 */
	public $startIPNum = null;
	public $endIPNum = null;
	public $locID = null;

	/**
	 * descripcion de la Tabla
	* @return multitype:string
	*/
	public function TableDescribe(){
		return array(
				'startIPNum' => 'int',
				'endIPNum' => 'int',
				'locID' => 'int',
		);
	}
	
}
?>