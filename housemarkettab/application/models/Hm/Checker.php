<?php 

class Hm_Checker extends Zend_Db_Table {
	
	public $request = '';
	
	protected $_name = 'propropiedad';
	
	function __construct() {
		$this->_setAdapter('db');
	}
	/**
	 * descripcion de la Tabla
	* @return multitype:string
	*/
	public function pageHasPropiedad($idPage,$mode = 'sale'){
		$action = array('S','B');
		if($mode == 'rent'){
			$action = array('R','B');
		}
		
		$select = $this->select();
		$select->setIntegrityCheck(false);
		$select->from('fb_page',array('uid','pageid'));
		$select->where('fb_page.pageid ='. $idPage);
		
		$select->join('propropiedad', 'fb_page.uid = propropiedad.Uid', array('CodigoPropiedad'));
		
		// mode sale or rent
		foreach($action as $val){
			$wherelist[] = 'propropiedad.Accion = \''. $val .'\' ';
		}
		$where = implode(' OR ', $wherelist);
		$select->where( $where);
		
		// control latitud longitud
		$select->where('propropiedad.Latitud IS NOT NULL');
		$select->where('propropiedad.Longitud IS NOT NULL');
		$select->where('propropiedad.FechaInactiva IS NULL');
		
		$this->request = $select->assemble();

		$rs = $this->fetchAll($select);
		
		if(empty($rs)){
			return false;
		}
		
		$result = $rs->toArray();
		if(is_array($result) && isset($result[0])){
			return true;
		}
		
		return false;
	}

}
?>