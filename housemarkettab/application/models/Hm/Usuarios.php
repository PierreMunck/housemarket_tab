<?php 

class Hm_Usuarios extends Hm_Db_Table {

	/**
	 * Nombre de la tabla
	 * @var String
	 */
	protected $_name = 'usuarios';

	/**
	 * Nombre de la llave primaria de la tabla
	 * @var Integer
	 */
	protected $_primary = 'idusuario';
	
	/**
	 * campo de la tabla
	 */
	public $idusuario = null;
	public $tipo = null;
	public $usuario = null;
	public $clave = null;
	public $nombre = null;
	public $apellido = null;
	public $estatus = null;
	public $email = null;

	/**
	 * descripcion de la Tabla
	* @return multitype:string
	*/
	public function TableDescribe(){
		return array(
				'idusuario' => 'int',
				'tipo' => 'string',
				'usuario' => 'string',
				'clave' => 'string',
				'nombre' => 'string',
				'apellido' => 'string',
				'estatus' => 'int',
				'email' => 'string',
		);
	}
}
?>