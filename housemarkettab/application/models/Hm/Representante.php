<?php 

class Hm_Representante extends Hm_Db_Table {

	/**
	 * Nombre de la tabla
	 * @var String
	 */
	protected $_name = 'representante';

	/**
	 * Nombre de la llave primaria de la tabla
	 * @var Integer
	 */
	protected $_primary = 'CodigoRepresenta';
	
	/**
	 * campo de la tabla
	 */
	public $CodigoRepresenta = null;
	public $NombreRepresenta = null;
	public $NombreEmpresa = null;
	public $Direccion = null;
	public $ZipCode = null;
	public $Telefono = null;
	public $EMail = null;
	public $Fax = null;
	public $idusuario = null;
	public $Uid = null;

	/**
	 * descripcion de la Tabla
	* @return multitype:string
	*/
	public function TableDescribe(){
		return array(
				'CodigoRepresenta' => 'int',
				'NombreRepresenta' => 'string',
				'NombreEmpresa' => 'string',
				'Direccion' => 'string',
				'ZipCode' => 'string',
				'Telefono' => 'string',
				'EMail' => 'string',
				'Fax' => 'string',
				'idusuario' => 'int',
				'Uid' => 'int',
		);
	}
	
}
?>