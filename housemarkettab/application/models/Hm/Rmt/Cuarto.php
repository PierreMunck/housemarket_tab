<?php 

class Hm_Rmt_Cuarto extends Hm_Db_Table {
	/**
	 * Nombre de la tabla
	 * @var String
	 */
	protected $_name = 'RMTCuarto';
	
	/**
	 * Nombre de la llave primaria de la tabla
	 * @var Integer
	 */
	protected $_primary = 'CodigoCuarto';
	
	/**
	 * campo de la tabla
	 */
	public $CodigoCuarto = null;
	public $Titulo = null;
	public $Descripcion = null;
	public $TipoAlojamiento = null;
	public $TipoPropiedad = null;
	public $MaxCapacidad = null;
	public $EspacioDisponible = null;
	public $TipoCuarto = null;
	public $TipoBano = null;
	public $MontoRenta = null;
	public $IncluyeServicio = null;
	public $MontoServicios = null;
	public $FechaDisponible = null;
	public $CortoPlazo = null;
	public $Direccion = null;
	public $Estado = null;
	public $Ciudad = null;
	public $ZipCode = null;
	public $Calle = null;
	public $CodigoPostal = null;
	public $Latitud = null;
	public $Longitud = null;
	public $Uid = null;
	public $FechaRegistro = null;
	public $FechaInactiva = null;
	public $EdadMinimaHabitan = null;
	public $EdadMaximaHabitan = null;
	public $FumadorHabitan = null;
	public $IdiomaHabitan = null;
	public $OcupacionHabitan = null;
	public $GeneroHabitan = null;
	public $MascotaHabitan = null;
	public $OrientacionHabitan = null;
	public $HijosHabitan = null;
	public $GeneroAcepta = null;
	public $ParejaAcepta = null;
	public $NinosAcepta = null;
	public $MascotaAcepta = null;
	public $FumadorAcepta = null;
	public $OrientacionAcepta = null;
	public $OcupacionAcepta = null;
	public $EdadMinimaAcepta = null;
	public $EdadMaximaAcepta = null;
	public $EstanciaMinima = null;
	public $EstadoRoommate = null;
	public $Correo = null;
	
	/**
	 * descripcion de la Tabla
	 * @return multitype:string
	 */
	public function TableDescribe(){
		return array(
				'CodigoCuarto' => 'int',
				'Titulo' => 'string',
				'Descripcion' => 'string',
				'TipoAlojamiento' => 'int',
				'TipoPropiedad' => 'int',
				'MaxCapacidad' => 'int',
				'EspacioDisponible' => 'int',
				'TipoCuarto' => 'int',
				'TipoBano' => 'int',
				'MontoRenta' => 'int',
				'IncluyeServicio' => 'string',
				'MontoServicios' => 'int',
				'FechaDisponible' => 'date',
				'CortoPlazo' => 'string',
				'Direccion' => 'string',
				'Estado' => 'string',
				'Ciudad' => 'string',
				'ZipCode' => 'string',
				'Calle' => 'string',
				'CodigoPostal' => 'string',
				'Latitud' => 'int',
				'Longitud' => 'int',
				'Uid' => 'string',
				'FechaRegistro' => 'date',
				'FechaInactiva' => 'date',
				'EdadMinimaHabitan' => 'int',
				'EdadMaximaHabitan' => 'int',
				'FumadorHabitan' => 'int',
				'IdiomaHabitan' => 'string',
				'OcupacionHabitan' => 'int',
				'GeneroHabitan' => 'int',
				'MascotaHabitan' => 'int',
				'OrientacionHabitan' => 'int',
				'HijosHabitan' => 'int',
				'GeneroAcepta' => 'int',
				'ParejaAcepta' => 'int',
				'NinosAcepta' => 'int',
				'MascotaAcepta' => 'int',
				'FumadorAcepta' => 'int',
				'OrientacionAcepta' => 'int',
				'OcupacionAcepta' => 'int',
				'EdadMinimaAcepta' => 'int',
				'EdadMaximaAcepta' => 'int',
				'EstanciaMinima' => 'int',
				'EstadoRoommate' => 'int',
				'Correo' => 'string',
		);
	}
	
}
?>