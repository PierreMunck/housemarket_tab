<?php 

class Hm_Rmt_Roommate extends Hm_Db_Table {
	/**
	 * Nombre de la tabla
	 * @var String
	 */
	protected $_name = 'RMTRoommate';
	
	/**
	 * Nombre de la llave primaria de la tabla
	 * @var Integer
	 */
	protected $_primary = 'CodigoRoommate';
	
	/**
	 * campo de la tabla
	 */
	public $CodigoRoommate = null;
	public $Titulo = null;
	public $Descripcion = null;
	public $TipoBusqueda = null;
	public $TipoPropiedad = null;
	public $MaxCapacidad = null;
	public $TipoCuarto = null;
	public $TipoBano = null;
	public $MontoRenta = null;
	public $FechaMudanza = null;
	public $Uid = null;
	public $FechaRegistro = null;
	public $FechaInactiva = null;
	public $CantBusca = null;
	public $EdadMinimaBusca = null;
	public $EdadMaximaBusca = null;
	public $FumadorBusca = null;
	public $IdiomaBusca = null;
	public $OcupacionBusca = null;
	public $GeneroBusca = null;
	public $MascotaBusca = null;
	public $OrientacionBusca = null;
	public $HijosBusco = null;
	public $GeneroAcepta = null;
	public $ParejaAcepta = null;
	public $NinosAcepta = null;
	public $MascotaAcepta = null;
	public $FumadorAcepta = null;
	public $OrientacionAcepta = null;
	public $OcupacionAcepta = null;
	public $EdadMinimaAcepta = null;
	public $EdadMaximaAcepta = null;
	public $EstanciaMinima = null;
	public $Amueblado = null;
	public $EstadoRoommate = null;
	public $Direccion = null;
	public $Estado = null;
	public $Ciudad = null;
	public $Calle = null;
	public $CodigoPostal = null;
	public $Latitud = null;
	public $Longitud = null;
	public $ZipCode = null;
	public $Correo = null;
	
	/**
	 * descripcion de la Tabla
	 * @return multitype:string
	 */
	public function TableDescribe(){
		return array(
				'CodigoRoommate' => 'int',
				'Titulo' => 'string',
				'Descripcion' => 'string',
				'TipoBusqueda' => 'int',
				'TipoPropiedad' => 'int',
				'MaxCapacidad' => 'int',
				'TipoCuarto' => 'int',
				'TipoBano' => 'int',
				'MontoRenta' => 'int',
				'FechaMudanza' => 'date',
				'Uid' => 'string',
				'FechaRegistro' => 'date',
				'FechaInactiva' => 'date',
				'CantBusca' => 'int',
				'EdadMinimaBusca' => 'int',
				'EdadMaximaBusca' => 'int',
				'FumadorBusca' => 'int',
				'IdiomaBusca' => 'string',
				'OcupacionBusca' => 'int',
				'GeneroBusca' => 'int',
				'MascotaBusca' => 'int',
				'OrientacionBusca' => 'int',
				'HijosBusco' => 'int',
				'GeneroAcepta' => 'int',
				'ParejaAcepta' => 'int',
				'NinosAcepta' => 'int',
				'MascotaAcepta' => 'int',
				'FumadorAcepta' => 'int',
				'OrientacionAcepta' => 'int',
				'OcupacionAcepta' => 'int',
				'EdadMinimaAcepta' => 'int',
				'EdadMaximaAcepta' => 'int',
				'EstanciaMinima' => 'int',
				'Amueblado' => 'int',
				'EstadoRoommate' => 'int',
				'Direccion' => 'string',
				'Estado' => 'string',
				'Ciudad' => 'string',
				'Calle' => 'string',
				'CodigoPostal' => 'string',
				'Latitud' => 'int',
				'Longitud' => 'int',
				'ZipCode' => 'string',
				'Correo' => 'string',
		);
	}
	
}
?>