<?php 

class Hm_Rmt_Foto extends Hm_Db_Table {
	/**
	 * Nombre de la tabla
	 * @var String
	 */
	protected $_name = 'RMTFoto';
	
	/**
	 * Nombre de la llave primaria de la tabla
	 * @var Integer
	 */
	protected $_primary = 'RMTFotoID';
	
	/**
	 * campo de la tabla
	 */
	public $RMTFotoID = null;
	public $CodigoCuarto = null;
	public $Principal = null;
	public $FotoGrande = null;
	public $Comentario = null;
	public $AltoFotoGrande = null;
	public $AnchoFotoGrande = null;
	public $FotoDestacada = null;
	public $FotoResultado = null;
	public $FotoGaleria = null;
	public $Mime = null;
	public $FechaCreacion = null;
	public $FechaModificacion = null;
	
	/**
	 * descripcion de la Tabla
	 * @return multitype:string
	 */
	public function TableDescribe(){
		return array(
				'RMTFotoID' => 'int',
				'CodigoCuarto' => 'int',
				'Principal' => 'bool',
				'FotoGrande' => 'file',
				'Comentario' => 'string',
				'AltoFotoGrande' => 'int',
				'AnchoFotoGrande' => 'int',
				'FotoDestacada' => 'file',
				'FotoResultado' => 'file',
				'FotoGaleria' => 'file',
				'Mime' => 'string',
				'FechaCreacion' => 'date',
				'FechaModificacion' => 'date',
		);
	}
	
}
?>