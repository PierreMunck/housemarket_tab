<?php 

class Hm_PhFileTypes extends Hm_Db_Table {

	/**
	 * Nombre de la tabla
	 * @var String
	 */
	protected $_name = 'phfiletypes';

	/**
	 * Nombre de la llave primaria de la tabla
	 * @var Integer
	 */
	protected $_primary = 'extension';
	
	/**
	 * campo de la tabla
	 */
	public $extension = null;
	public $mime = null;
	public $content = null;
	public $player = null;
	public $disabled = null;

	/**
	 * descripcion de la Tabla
	* @return multitype:string
	*/
	public function TableDescribe(){
		return array(
				'extension' => 'string',
				'mime' => 'string',
				'content' => 'string',
				'player' => 'string',
				'disabled' => 'int',
		);
	}

	
}
?>