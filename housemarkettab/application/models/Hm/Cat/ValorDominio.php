<?php

class Hm_Cat_ValorDominio extends Hm_Db_Table {
	/**
	 * Nombre de la tabla
	 * @var String
	 */
	protected $_name = 'CATValorDominio';
	
	/**
	 * Nombre de la llave primaria de la tabla
	 * @var Integer
	 */
	protected $_primary = 'CodigoValor';
	
	/**
	 * campo de la tabla
	 */
	public $CodigoValor = null;
	public $CodigoDominio = null;
	public $NombreValor = null;
	public $Estado = null;
	
	/**
	 * descripcion de la Tabla
	 * @return multitype:string
	 */
	public function TableDescribe(){
		return array(
				'CodigoValor' => 'int',
				'CodigoDominio' => 'int',
				'NombreValor' => 'string',
				'Estado' => 'string',
		);
	}
	
}
?>