<?php 

class Hm_Cat_Moneda extends Hm_Db_Table {
	/**
	 * Nombre de la tabla
	 * @var String
	 */
	protected $_name = 'catmoneda';
	
	/**
	 * Nombre de la llave primaria de la tabla
	 * @var Integer
	 */
	protected $_primary = 'CodigoMoneda';
	
	/**
	 * campo de la tabla
	 */
	public $CodigoMoneda = null;
	public $SimboloMoneda = null;
	public $NombreMoneda = null;
	public $NombreEN = null;
	public $CodigoEstandar = null;
	
	/**
	 * descripcion de la Tabla
	 * @return multitype:string
	 */
	public function TableDescribe(){
		return array(
				'CodigoMoneda' => 'int',
				'SimboloMoneda' => 'string',
				'NombreMoneda' => 'string',
				'NombreEN' => 'string',
				'CodigoEstandar' => 'string',
		);
	}
	
}
?>