<?php 

class Hm_Cat_CategoriaIdioma extends Hm_Db_Table {
	/**
	 * Nombre de la tabla
	 * @var String
	 */
	protected $_name = 'catcategoriaidioma';
	
	/**
	 * Nombre de la llave primaria de la tabla
	 * @var Integer
	 */
	protected $_primary = 'CATCategoriaIdiomaID';
	
	/**
	 * campo de la tabla
	 */
	public $CATCategoriaIdiomaID = null;
	public $CodigoCategoria = null;
	public $CodigoIdioma = null;
	public $ValorIdioma = null;
	
	/**
	 * descripcion de la Tabla
	 * @return multitype:string
	 */
	public function TableDescribe(){
		return array(
				'CATCategoriaIdiomaID' => 'int',
				'CodigoCategoria' => 'int',
				'CodigoIdioma' => 'string',
				'ValorIdioma' => 'string',
		);
	}
	
}
?>