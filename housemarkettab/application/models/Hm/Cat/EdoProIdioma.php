<?php 

class Hm_Cat_EdoProIdioma extends Hm_Db_Table {
	/**
	 * Nombre de la tabla
	 * @var String
	 */
	protected $_name = 'catedoproidioma';
	
	/**
	 * Nombre de la llave primaria de la tabla
	 * @var Integer
	 */
	protected $_primary = 'CATEdoProIdiomaID';
	
	/**
	 * campo de la tabla
	 */
	public $CATEdoProIdiomaID = null;
	public $CodigoEdoPropiedad = null;
	public $CodigoIdioma = null;
	public $ValorIdioma = null;
	
	/**
	 * descripcion de la Tabla
	 * @return multitype:string
	 */
	public function TableDescribe(){
		return array(
				'CATEdoProIdiomaID' => 'int',
				'CodigoEdoPropiedad' => 'int',
				'CodigoIdioma' => 'string',
				'ValorIdioma' => 'string',
		);
	}
	
}
?>