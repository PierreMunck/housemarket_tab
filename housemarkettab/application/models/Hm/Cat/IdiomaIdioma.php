<?php 

class Hm_Cat_IdiomaIdioma extends Hm_Db_Table {
	/**
	 * Nombre de la tabla
	 * @var String
	 */
	protected $_name = 'catidiomaidioma';
	
	/**
	 * Nombre de la llave primaria de la tabla
	 * @var Integer
	 */
	protected $_primary = 'CATIdiomaIdiomaID';
	
	/**
	 * campo de la tabla
	 */
	public $CATIdiomaIdiomaID = null;
	public $CodigoIdiomaOrigen = null;
	public $CodigoIdiomaDestino = null;
	public $ValorIdioma = null;
	
	/**
	 * descripcion de la Tabla
	 * @return multitype:string
	 */
	public function TableDescribe(){
		return array(
				'CATIdiomaIdiomaID' => 'int',
				'CodigoIdiomaOrigen' => 'string',
				'CodigoIdiomaDestino' => 'string',
				'ValorIdioma' => 'string',
		);
	}
	
}
?>