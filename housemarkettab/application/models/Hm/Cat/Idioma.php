<?php 

class Hm_Cat_Idioma extends Hm_Db_Table {
	/**
	 * Nombre de la tabla
	 * @var String
	 */
	protected $_name = 'catidioma';
	
	/**
	 * Nombre de la llave primaria de la tabla
	 * @var Integer
	 */
	protected $_primary = 'CodigoIdioma';
	
	/**
	 * campo de la tabla
	 */
	public $CodigoIdioma = null;
	public $NombreIdioma = null;
	public $Activo = null;
	
	/**
	 * descripcion de la Tabla
	 * @return multitype:string
	 */
	public function TableDescribe(){
		return array(
				'CodigoIdioma' => 'string',
				'NombreIdioma' => 'string',
				'Activo' => 'string',
		);
	}
	
}
?>