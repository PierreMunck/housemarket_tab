<?php 

class Hm_Cat_EdoPropiedad extends Hm_Db_Table {
	/**
	 * Nombre de la tabla
	 * @var String
	 */
	protected $_name = 'catedopropiedad';
	
	/**
	 * Nombre de la llave primaria de la tabla
	 * @var Integer
	 */
	protected $_primary = 'CodigoEdoPropiedad';
	
	/**
	 * campo de la tabla
	 */
	public $CodigoEdoPropiedad = null;
	public $NombreEdoPropiedad = null;
	public $NombreEN = null;
	
	/**
	 * descripcion de la Tabla
	 * @return multitype:string
	 */
	public function TableDescribe(){
		return array(
				'CodigoEdoPropiedad' => 'int',
				'NombreEdoPropiedad' => 'string',
				'NombreEN' => 'string',
		);
	}
	
}
?>