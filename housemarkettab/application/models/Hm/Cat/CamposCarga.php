<?php 

class Hm_Cat_CamposCarga extends Hm_Db_Table {
	/**
	 * Nombre de la tabla
	 * @var String
	 */
	protected $_name = 'catcamposcarga';
	
	/**
	 * Nombre de la llave primaria de la tabla
	 * @var Integer
	 */
	protected $_primary = 'IDCamposCarga';
	
	/**
	 * campo de la tabla
	 */
	public $IDCamposCarga = null;
	public $CampoArchivo = null;
	public $CampoTabla = null;
	
	/**
	 * descripcion de la Tabla
	 * @return multitype:string
	 */
	public function TableDescribe(){
		return array(
				'IDCamposCarga' => 'int',
				'CampoArchivo' => 'string',
				'CampoTabla' => 'string',
		);
	}
	
}

?>