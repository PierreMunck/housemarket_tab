<?php

class Hm_Cat_Atributo extends Hm_Db_Table {
    /**
     * Nombre de la tabla
     * @var String
     */
    protected $_name = 'catatributo';

    /**
     * Nombre de la llave primaria de la tabla
     * @var Integer
     */
    protected $_primary = 'CodigoAtributo';

    
    /**
     * campo de la tabla
     */
    public $CodigoAtributo = null;
    public $NombreAtributo = null;
    public $TipoAtributo = null;
    public $TamanioAtributo = null;
    public $LongDecimal = null;
    public $NombreEN = null;
    
    /**
     * descripcion de la Tabla
     * @return multitype:string
     */
    public function TableDescribe(){
    	return array(
    		'CodigoAtributo' => 'int',
    		'NombreAtributo' => 'string',
    		'TipoAtributo' => 'string',
    		'TamanioAtributo' => 'int',
    		'LongDecimal' => 'int',
    		'NombreEN' => 'string',
    		
    	);
    }
    
    function __construct($catAtId = null) {
    	if(is_int($catAtId)){
    		$this->CodigoAtributo = $catAtId;
    	}
    	parent::__construct();
    }

}

?>