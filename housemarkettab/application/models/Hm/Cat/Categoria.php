<?php 

class Hm_Categoria extends Hm_Db_Table {
	/**
	 * Nombre de la tabla
	 * @var String
	 */
	protected $_name = 'catcategoria';
	
	/**
	 * Nombre de la llave primaria de la tabla
	 * @var Integer
	 */
	protected $_primary = 'CodigoCategoria';
	
	/**
	 * campo de la tabla
	 */
	public $CodigoCategoria = null;
	public $NombreCategoria = null;
	public $NombreEN = null;
	public $filtro = null;
	public $orden = null;
	
	/**
	 * descripcion de la Tabla
	 * @return multitype:string
	 */
	public function TableDescribe(){
		return array(
				'CodigoCategoria' => 'int',
				'NombreCategoria' => 'string',
				'NombreEN' => 'string',
				'filtro' => 'int',
				'orden' => 'int',
		);
	}
	
}
?>