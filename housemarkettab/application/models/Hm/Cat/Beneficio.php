<?php 
class Hm_Cat_Beneficio extends Hm_Db_Table {
	/**
	 * Nombre de la tabla
	 * @var String
	 */
	protected $_name = 'catbeneficio';
	
	/**
	 * Nombre de la llave primaria de la tabla
	 * @var Integer
	 */
	protected $_primary = 'CodigoBeneficio';
	
	/**
	 * campo de la tabla
	 */
	public $CodigoBeneficio = null;
	public $NombreBeneficio = null;
	public $NombreEN = null;
	
	/**
	 * descripcion de la Tabla
	 * @return multitype:string
	 */
	public function TableDescribe(){
		return array(
				'CodigoBeneficio' => 'int',
				'NombreBeneficio' => 'string',
				'NombreEN' => 'string',
		);
	}
	
}
?>