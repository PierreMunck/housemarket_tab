<?php 

class Hm_Cat_AtributoIdioma extends Hm_Db_Table {
	/**
	 * Nombre de la tabla
	 * @var String
	 */
	protected $_name = 'catatributoidioma';
	
	/**
	 * Nombre de la llave primaria de la tabla
	 * @var Integer
	 */
	protected $_primary = 'CATAtributoIdiomaID';
	
	/**
	 * campo de la tabla
	 */
	public $CATAtributoIdiomaID = null;
	public $CodigoAtributo = null;
	public $CodigoIdioma = null;
	public $ValorIdioma = null;
	
	/**
	 * descripcion de la Tabla
	 * @return multitype:string
	 */
	public function TableDescribe(){
		return array(
				'CATAtributoIdiomaID' => 'int',
				'CodigoAtributo' => 'int',
				'CodigoIdioma' => 'string',
				'ValorIdioma' => 'string',
		);
	}
	
}
?>