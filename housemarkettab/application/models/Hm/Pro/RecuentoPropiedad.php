<?php 
class Hm_Pro_RecuentoPropiedad extends Hm_Db_Table {
	/**
	 * Nombre de la tabla
	 * @var String
	 */
	protected $_name = 'RecuentoPropiedad';
	
	/**
	 * Nombre de la llave primaria de la tabla
	 * @var Integer
	 */
	protected $_primary = 'RecuentoID';
	
	/**
	 * campo de la tabla
	 */
	public $RecuentoID = null;
	public $FechaHora = null;
	public $Cantidad = null;
	
	/**
	 * descripcion de la Tabla
	 * @return multitype:string
	 */
	public function TableDescribe(){
		return array(
				'RecuentoID' => 'int',
				'FechaHora' => 'date',
				'Cantidad' => 'int',
		);
	}
	
}
?>