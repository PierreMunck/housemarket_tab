<?php

class Hm_Pro_AtributoPropiedad extends Hm_Db_Table {

    /**
     * Nombre de la tabla de propiedad con la cual se asocia
     * @var String
     */
    protected $_name = 'proatributopropiedad';
    /**
     * Llave primaria de la tabla de propiedad
     * @var String
     */
    protected $_primary = 'CodigoPropiedad';
    //CodigoPropiedad
    //CodigoAtributo

    /**
     * campo de la tabla
     */
    
    public $CodigoPropiedad = null;
    public $CodigoBeneficio = null;
    
    /**
     * descripcion de la Tabla
     * @return multitype:string
     */
    public function TableDescribe(){
    	return array(
    			'CodigoPropiedad' => 'int',
    			'CodigoBeneficio' => 'int',
    			);
    }

    function __construct($proId = null, $benefId = null) {
    	if(is_int($proId) && is_int($benefId)){
    		$this->CodigoPropiedad = $proId;
    		$this->CodigoBeneficio = $benefId;
    	}
    	parent::__construct();
    }

}

?>