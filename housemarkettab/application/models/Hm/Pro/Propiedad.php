<?php

class Hm_Pro_Propiedad extends Hm_Db_Table {
    /**
     * Factor de conversion de varas cuadradas a metros cuadrados
     */
    const CONV_VARA_CUADRADA_A_METRO = 0.702579;

    /**
     * Factor de conversion de pies cuadrados a metros cuadrados
     */
    const CONV_PIE_CUADRADO_A_METRO = 0.092903;

    /**
     * Nombre de la tabla de propiedad con la cual se asocia
     * @var String
     */
    protected $_name = 'propropiedad';
    /**
     * Llave primaria de la tabla de propiedad
     * @var String
     */
    protected $_primary = 'CodigoPropiedad';

    
    protected $_dependentTables = array(
        'catpais' => array(
        		'type' => 'left',
        		'value' => 'propropiedad.ZipCode = catpais.ZipCode',
        		),
    	'catmoneda' => array(
        		'type' => 'left',
        		'value' => 'catpais.CodigoMoneda = catmoneda.CodigoMoneda',
        		),
    	'catcategoria' => array(
        		'type' => 'strict',
        		'value' => 'propropiedad.CodigoCategoria = catcategoria.CodigoCategoria',
        		),
    	'catcategoriaidioma' => array(
        		'type' => 'strict',
        		'value' => 'catcategoria.CodigoCategoria = catcategoriaidioma.CodigoCategoria',
        		),
    	'cattipoenlista' => array(
        		'type' => 'strict',
        		'value' => 'propropiedad.Accion = cattipoenlista.CodigoEnlista',
        		),
    	'catenlistaidioma' => array(
        		'type' => 'strict',
        		'value' => 'cattipoenlista.Codigoenlista = catenlistaidioma.CodigoEnlista',
        		),
    	'catidioma' => array(
        		'type' => 'strict',
        		'value' => 'catcategoriaidioma.CodigoIdioma = catidioma.CodigoIdioma',
        		),
        'atributopropiedad' => array(
        		'type' => 'left',
        		'value' => 'propropiedad.CodigoPropiedad = atributopropiedad.CodigoPropiedad',
        		),
    );
    

    public function getDependentTables(){
    	$dependentTables = $this->_dependentTables;
    	$idioma = Zend_Registry::get('language');
    	$dependentTables['catcategoriaidioma']['where'] = array(
    				'field' => 'CodigoIdioma',
    				'value' => $idioma,
    			);
    	$dependentTables['catenlistaidioma']['where'] = array(
    			'field' => 'CodigoIdioma',
    			'value' => $idioma,
    	);
    	return $dependentTables;
    }
    
    /**
     * campo de la tabla
     */
    public $CodigoBroker = null;
    public $CodigoPropiedad = null;
    public $NombrePropiedad = null;
    public $DescPropiedad = null;
    public $Direccion = null;
    public $Estado = null;
    public $Ciudad = null;
    public $ZipCode = null;
    public $Latitud = null;
    public $Longitud = null;
    public $CodigoCategoria = null;
    public $FechaRegistro = null;
    public $FechaPublica = null;
    public $FechaModifica = null;
    public $Accion = null;
    public $CodigoMoneda = null;
    public $PrecioVenta = null;
    public $PrecioAlquiler = null;
    public $Uid = null;
    public $CodigoEdoPropiedad = null;
    public $CargaMasiva = null;
    public $Calle = null;
    public $CodigoPostal = null;
    public $AreaLote = null;
    public $Area = null;
    public $UnidadMedida = null;
    public $AreaLoteMt2 = null;
    public $AreaMt2 = null;
    public $UnidadMedidaLote = null;
    public $FechaInactiva = null;
    
    /**
     * joined campo
     */
    public $AtributoCuarto = null;
    public $AtributoBano = null;
    public $AtributoParqueo = null;
    public $AtributoMascota = null;
    public $Pais = null;
    public $SimboloMoneda = null;
    public $Transaccion = null;
    public $MonedaCodidoEstandar = null;
    public $ListAttributo = null;
    public $detallePrecioVenta = null;
    public $detallePrecioVentaSafe;
    public $detallePrecioAlquiler = null;
	public $detallePrecioAlquilerSafe = null;
	public $NombreCategoria = null;
    
    /**
     * descripcion de la Tabla
     * @return multitype:string
     */
    public function TableDescribe(){
    	return array(
    			'CodigoBroker' => 'string',
    			'CodigoPropiedad' => 'int',
    			'NombrePropiedad' => 'string',
    			'DescPropiedad' => 'string',
    			'Direccion' => 'string',
    			'Estado' => 'string',
    			'Ciudad' => 'string',
    			'ZipCode' => 'string',
    			'Latitud' => 'int',
    			'Longitud' => 'int',
    			'CodigoCategoria' => 'int',
    			'FechaRegistro' => 'date',
    			'FechaPublica' => 'date',
    			'FechaModifica' => 'date',
    			'Accion' => 'string',
    			'CodigoMoneda' => 'int',
    			'PrecioVenta' => 'int',
    			'PrecioAlquiler' => 'int',
    			'Uid' => 'int',
    			'CodigoEdoPropiedad' => 'int',
    			'CargaMasiva' => 'bool',
    			'Calle' => 'string',
    			'CodigoPostal' => 'string',
    			'AreaLote' => 'int',
    			'Area' => 'int',
    			'UnidadMedida' => 'string',
    			'AreaLoteMt2' => 'int',
    			'AreaMt2' => 'int',
    			'UnidadMedidaLote' => 'string',
    			'FechaInactiva' => 'date',
    			);
    }

    function __construct($propId = null) {
    	if(is_int($propId)){
    		$this->CodigoPropiedad = $propId;
    	}
    	parent::__construct();
    }

    public function populate($values) {
    	parent::populate($values);

      	$caracteristicasprop = '';
      	$this->Pais = $values['Pais'];
      	$this->LabelArea = ($this->Area > 0) ? intval($this->Area) . ' ' . $this->UnidadMedida : "-";
      	$this->LabelAreaLote = ($this->AreaLote > 0) ? intval($this->AreaLote) . ' ' . $this->UnidadMedidaLote : "-";
      	
      	$LabelLocation = array();
      	if(isset($this->Ciudad)){
      		$LabelLocation[] = $this->Ciudad;
      	}
      	if(isset($this->Estado)){
      		// no estado en los dise�o
      		//$LabelLocation[] = $this->Estado;
      	}
      	if(isset($this->Pais)){
      		$LabelLocation[] = $this->Pais;
      	}
      	$this->LabelLocation = implode(", ",$LabelLocation).
      	
      	$this->AtributoCuarto = intval($values['cuarto']);
      	$this->AtributoBano = intval($values['bano']);
      	$this->AtributoParqueo = intval($values['parqueo']);
      	$this->AtributoMascota = $values['mascota'];
      	$this->SimboloMoneda = $values['SimboloMoneda'];
      	$this->MonedaCodidoEstandar = $values['CodigoEstandar'];
      	$this->Beneficios = "";
      	$this->NombreCategoria = $values['NombreCategoria'];
      	
      	$translator = Zend_Registry::get('Zend_Translate');
      	
      	switch ($this->Accion){
      		case 'S':
      			$this->Transaccion = $translator->getAdapter()->translate('LABEL_FOR_LOWERCASE') .' '. $values['ValorIdioma'] .' US $'. $this->PrecioVenta;
      			break;
      		 
      		case 'R':
      		 	$this->Transaccion = $translator->getAdapter()->translate('LABEL_FOR_LOWERCASE') .' '. $values['ValorIdioma'] .' US $'. $this->PrecioAlquiler;
      			break; 
      		case 'F':
      		 	$this->Transaccion = $translator->getAdapter()->translate('LABEL_FOR_LOWERCASE') .' '. $values['ValorIdioma'] .' US $'. $this->PrecioVenta;
      			break; 
      		case 'B':
      		 	$this->Transaccion = $translator->getAdapter()->translate('LABEL_FOR_LOWERCASE') .' '. $translator->getAdapter()->translate('LABEL_SALE') .' US $'. $this->PrecioVenta .' '. $translator->getAdapter()->translate('LABEL_OR') .' '. $translator->getAdapter()->translate('LABEL_FOR_LOWERCASE') .' '. $translator->getAdapter()->translate('LABEL_RENT') .' US $'. $this->PrecioAlquiler;
      			break; 
      	}
      	
      	if($this->CodigoEdoPropiedad != 4){
      		$attributos = new Hm_Pro_AtributoPropiedadList();
      		$attributos->search(array('CodigoPropiedad' => $this->CodigoPropiedad));
      		$this->ListAttributo = $attributos;
      	}
      	
      	$this->detallePrecio();
    	
    }
    
    
    private function detallePrecio(){
    	//@todo hacer que ese codigo no sea necesario en la construci�n del detalle creando directamente la variable $propertyDetails
		if (!empty($this->PrecioVenta)) {
			$detallePrecioVenta = $this->SimboloMoneda .
			$this->clearPrecio($this->PrecioVenta) . 
			' <abbr title="<i18n>ISO_4217_CURRENCY_CODE_' . $this->MonedaCodidoEstandar . '</i18n>">'.
			$this->MonedaCodidoEstandar .
			'</abbr>';
		}
    	if (!empty($this->PrecioAlquiler)) {
			$detallePrecioAlquiler = $this->SimboloMoneda .
			$this->clearPrecio($this->PrecioAlquiler) .
			' <abbr title="<i18n>ISO_4217_CURRENCY_CODE_' . $this->MonedaCodidoEstandar . '</i18n>">'.
			$this->MonedaCodidoEstandar .
			'</abbr>';
		}
    	if (!empty($detallePrecioVenta)){
    		$this->detallePrecioVenta = $detallePrecioVenta;
    		$this->detallePrecioVentaSafe = strip_tags($this->detallePrecioVenta);
    	}
    		
    	if (!empty($detallePrecioAlquiler)){
    		$this->detallePrecioAlquiler = $detallePrecioAlquiler;
    		$this->detallePrecioAlquilerSafe = strip_tags($this->detallePrecioAlquiler);
    	}
    }
    
    public function validate(){
    	if(isset($this->CodigoPropiedad) && isset($this->NombrePropiedad)){
    			return true;
    	}
    	return false;
    }
    
    public function check(){
    	if($this->Latitud == null){
    		return false;
    	}
    	if($this->Longitud == null){
    		return false;
    	}
    	if($this->PrecioVenta == null && $this->PrecioAlquiler == null){
    		return false;
    	}
    	return parent::check();
    }
    //TODO
    public static function checkLimit($uid) {
    	$count_sql = "SELECT COUNT(*) num_prop FROM propiedad WHERE uid= ?";
    }
    
    private function clearPrecio($val){
    	$val = floatval($val);
    	return number_format($val);
    }
}

?>