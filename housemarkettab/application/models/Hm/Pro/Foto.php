<?php

class Hm_Pro_Foto extends Hm_Db_Table {

    /**
     * Nombre de la tabla de propiedad con la cual se asocia
     * @var String
     */
    protected $_name = 'profoto';
    /**
     * Llave primaria de la tabla de propiedad
     * @var String
     */
    protected $_primary = 'PROFotoID';

    /**
     * joined campo
     */
    private $urls = array();
    
    public $_PHOTO_TYPES = array(
    		"Principal" => 1,
    		"Destacada" => 2,
    		"Resultado" => 3,
    		"Galeria" => 4
    );
    
    /** columns restriction **/
    protected $_restrictColumns = array(
    		'PROFotoID',
    		'CodigoPropiedad',
    		'Principal',
    		'Comentario',
    		'AltoFotoGrande',
    		'AnchoFotoGrande',
    		'Mime',
    		'FechaCreacion',
    		'FechaModifica',
    );
    
    /**
     * campo de la tabla
     */
    public $PROFotoID = null;
    public $CodigoPropiedad = null;
    public $Principal = null;
    public $FotoGrande = null;
    public $Comentario = null;
    public $AltoFotoGrande = null;
    public $AnchoFotoGrande = null;
    public $FotoDestacada = null;
    public $FotoResultado = null;
    public $FotoGaleria = null;
    public $Mime = null;
    public $FechaCreacion = null;
    public $FechaModifica = null;

   
    public function getUrl($type = null){
    	if(!isset($type)){
    		$type = "Principal";
    	}
    	if(!isset($this->urls[$type])){
	    	$cfg = Zend_Registry::get('config');
	    	$ext = '.jpg';
	    	switch($this->Mime){
	    		case 'image/jpeg':
	    			$ext = '.jpg';
	    	}
	    	$url = $cfg['img_src_url'] .'pic'. $this->PROFotoID .'_'. $this->_PHOTO_TYPES[$type] . $ext;
	    	$this->urls[$type] = $url;
    	}
    	return $this->urls[$type];
    }
    
    /**
     * descripcion de la Tabla
     * @return multitype:string
     */
    public function TableDescribe(){
    	return array(
    			'PROFotoID' => 'int',
    			'CodigoPropiedad' => 'int',
    			'Principal' => 'bool',
    			'FotoGrande' => 'file',
    			'Comentario' => 'string',
    			'AltoFotoGrande' => 'int',
    			'AnchoFotoGrande' => 'int',
    			'FotoDestacada' => 'file',
    			'FotoResultado' => 'file',
    			'FotoGaleria' => 'file',
    			'Mime' => 'string',
    			'FechaCreacion' => 'date',
    			'FechaModifica' => 'date',
    			);
    }

    function __construct($proFotoId = null) {
    	if(is_int($proFotoId)){
    		$this->PROFotoID = $proFotoId;
    	}
    	if(is_array($proFotoId)){
    		parent::__construct($proFotoId);
    	}else{
    		parent::__construct();
    	}
    }

}

?>