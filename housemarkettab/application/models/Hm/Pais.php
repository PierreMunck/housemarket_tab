<?php 

class Hm_Pais extends Hm_Db_Table {

	/**
	 * Nombre de la tabla
	 * @var String
	 */
	protected $_name = 'pais';

	/**
	 * campo de la tabla
	 */
	public $zip_code = null;
	public $pais = null;

	/**
	 * descripcion de la Tabla
	* @return multitype:string
	*/
	public function TableDescribe(){
		return array(
				'zip_code' => 'string',
				'pais' => 'string',
		);
	}
}
?>