<?php 

class Hm_Cre_Promocion extends Hm_Db_Table {

	/**
	 * Nombre de la tabla
	 * @var String
	 */
	protected $_name = 'promocion';

	/**
	 * Nombre de la llave primaria de la tabla
	 * @var Integer
	 */
	protected $_primary = 'CodigoPromocion';
	
	/**
	 * campo de la tabla
	 */
	public $CodigoPromocion = null;
	public $NombrePromocion = null;
	public $Descripcion = null;
	public $CantidadCredito = null;
	public $FehaPromo = null;
	public $FechaVence = null;
	public $CodigoEdoPromocion = null;

	/**
	 * descripcion de la Tabla
	* @return multitype:string
	*/
	public function TableDescribe(){
		return array(
				'CodigoPromocion' => 'int',
				'NombrePromocion' => 'string',
				'Descripcion' => 'string',
				'CantidadCredito' => 'int',
				'FehaPromo' => 'date',
				'FechaVence' => 'date',
				'CodigoEdoPromocion' => 'string',
		);
	}
	
}
?>