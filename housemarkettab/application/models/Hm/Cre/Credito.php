<?php 

class Hm_Cre_Credito extends Hm_Db_Table {

	/**
	 * Nombre de la tabla
	 * @var String
	 */
	protected $_name = 'crecredito';

	/**
	 * Nombre de la llave primaria de la tabla
	 * @var Integer
	 */
	protected $_primary = 'CodigoCredito';
	
	/**
	 * campo de la tabla
	 */
	public $CodigoCredito = null;
	public $FechaCompra = null;
	public $FechaVence = null;
	public $Cantidad = null;
	public $Saldo = null;
	public $EstadoCredito = null;
	public $CodigoRepresentante = null;
	public $CodigoCliente = null;
	public $FechaPedido = null;
	public $MontoCompra = null;
	public $TipoCompra = null;
	public $CodigoPromocion = null;

	/**
	 * descripcion de la Tabla
	* @return multitype:string
	*/
	public function TableDescribe(){
		return array(
				'CodigoCredito' => 'int',
				'FechaCompra' => 'date',
				'FechaVence' => 'date',
				'Cantidad' => 'int',
				'Saldo' => 'int',
				'EstadoCredito' => 'string',
				'CodigoRepresentante' => 'int',
				'CodigoCliente' => 'int',
				'FechaPedido' => 'date',
				'MontoCompra' => 'int',
				'TipoCompra' => 'string',
				'CodigoPromocion' => 'int',
		);
	}
	
}
?>