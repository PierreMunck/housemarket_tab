<?php 

class Hm_Session extends Hm_Db_Table {

	/**
	 * Nombre de la tabla
	 * @var String
	 */
	protected $_name = 'Session';

	/**
	 * Nombre de la llave primaria de la tabla
	 * @var Integer
	 */
	protected $_primary = array('Uid');
	
	/**
	 * campo de la tabla
	 */

	public $Uid = null;
	public $Name = null;
	public $Installed = null;
	public $EMail = null;
	public $FechaSession = null;
	public $SessionExpire = null;

	/**
	 * descripcion de la Tabla
	* @return multitype:string
	*/
	public function TableDescribe(){
		return array(
				'Uid' => 'int',
				'Name' => 'string',
				'Installed' => 'int',
				'EMail' => 'string',
				'FechaSession' => 'date',
				'SessionExpire' => 'date',
		);
	}
	
	public function save() {
		
		if(!isset($this->Uid)){
			return false;
		}
		
		$where = 'Uid = '. $this->Uid;
		
		$select = $this->select();
		$select->setIntegrityCheck(false);
		$select->from('Session',array('Uid'));
		$select->where($where);
		$rs = $this->fetchAll($select);
		$updaterequest = false;
		if(!empty($rs)){
			$result = $rs->toArray();
			if(is_array($result) && isset($result[0])){
				$updaterequest = true;
			}
		}
		
		// charging data
		$data = array();
	
		foreach($this->TableDescribe() as $key => $type){
			$data[$key] = $this->safeval($this->{$key},$type);
		}
		try{
			if($updaterequest){
				$this->update($data,$where);
				//$this->log->info('update');
			}else{
				$this->insert($data);
				//$this->log->info('insert');
			}
		}catch (Exception $e){
			$this->log->err( __FILE__ .' '. __LINE__ .' '. print_r($e,true));
		}
	}

}
?>